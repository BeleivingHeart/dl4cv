import tensorflow.contrib.slim as slim
import pickle
import tensorflow as tf
from .misc import show
import numpy as np
import os

def loss(self, net_out):
    """
    Takes net.out and placeholders value
    returned in batch() func above,
    to build train_op and loss
    """
    # meta
    m = self.meta
    # scalar coefficients
    sprob = float(m['class_scale']) # coefficient for L(obj)(i), default 1.
    sconf = float(m['object_scale']) # coefficient for L(obj)(ij), default 1.
    snoob = float(m['noobject_scale']) # lambda_noobj, default 0.5
    scoor = float(m['coord_scale']) # lambda_coord, default 5

    S, B, C = m['side'], m['num'], m['classes']
    SS = S * S # number of grid cells

    print('{} loss hyper-parameters:'.format(m['model']))
    print('\tside    = {}'.format(m['side']))
    print('\tbox     = {}'.format(m['num']))
    print('\tclasses = {}'.format(m['classes']))
    print('\tscales  = {}'.format([sprob, sconf, snoob, scoor]))

    size1 = [None, SS, C]
    size2 = [None, SS, B]

    # return the below placeholders
    _probs = tf.placeholder(tf.float32, size1) # true class probabilities, one-hot
    _confs = tf.placeholder(tf.float32, size2) # 1 or 0, indicates whether an object actually exits
    _coord = tf.placeholder(tf.float32, size2 + [4]) # actual bounding-box coordinates
    # weights term for L2 loss
    _proid = tf.placeholder(tf.float32, size1) # whether a cell actually contains an object, L(obj)(i)
    # material calculating IOU
    _areas = tf.placeholder(tf.float32, size2) # actual bounding-box areas
    # convenient params for computing intersection areas
    _upleft = tf.placeholder(tf.float32, size2 + [2]) # actual bounding-box upleft
    _botright = tf.placeholder(tf.float32, size2 + [2]) # actual bounding-box botright

    self.placeholders = {
        'probs':_probs, 'confs':_confs, 'coord':_coord, 'proid':_proid,
        'areas':_areas, 'upleft':_upleft, 'botright':_botright
    }

    # Extract the coordinate prediction from net.out
    coords = net_out[:, SS * (C + B):]
    coords = tf.reshape(coords, [-1, SS, B, 4])
    wh = tf.pow(coords[:,:,:,2:4], 2) * S # unit: grid cell
    area_pred = wh[:,:,:,0] * wh[:,:,:,1] # unit: grid cell^2， 【batch, SS, B]
    centers = coords[:,:,:,0:2] # [batch, SS, B, 2]
    floor = centers - (wh * .5) # [batch, SS, B, 2]
    ceil  = centers + (wh * .5) # [batch, SS, B, 2]

    # calculate the intersection areas
    intersect_upleft   = tf.maximum(floor, _upleft) # [batch, SS, B, 2]
    intersect_botright = tf.minimum(ceil , _botright) # [batch, SS, B, 2]
    intersect_wh = intersect_botright - intersect_upleft # [batch, SS, B, 2]
    intersect_wh = tf.maximum(intersect_wh, 0.0) # [batch, SS, B, 2]
    intersect = tf.multiply(intersect_wh[:,:,:,0], intersect_wh[:,:,:,1]) # [batch, SS, B]

    # calculate the best IOU, set 0.0 confidence for worse boxes
    iou = tf.truediv(intersect, _areas + area_pred - intersect) # [batch, SS, B]
    best_box = tf.equal(iou, tf.reduce_max(iou, [2], True)) # [batch, SS, B]
    best_box = tf.to_float(best_box) # [batch, SS, B]
    confs = tf.multiply(best_box, _confs) # true confidence: if the box is the best box, its ideal confidence should be 1
                                            # else 0: A. not the best box
                                            #         B. the object dosen't even exist
                                            # [batch, SS, B]
    # take care of the weight terms
    conid = snoob * (1. - confs) + sconf * confs # Either there is no object in the first place
                                                # or the predicted bounding-box is not the most responsible,
                                                # the forward part will not be zero and the latter part zero
    weight_coo = tf.concat(4 * [tf.expand_dims(confs, -1)], 3) # [batch, SS ,B ,4], L(obj)(i,j)
    cooid = scoor * weight_coo # [batch,SS,B,4] lambda coord * L(obj)(i,j)
    proid = sprob * _proid # [batch,SS,C]

    # flatten 'em all
    probs = slim.flatten(_probs) # true class-specific probabilities
    proid = slim.flatten(proid) # L(obj)(i), class-specific weights
    confs = slim.flatten(confs) # true confidence
    conid = slim.flatten(conid) # confidence weights (two cases)
    coord = slim.flatten(_coord) # true coordinates
    cooid = slim.flatten(cooid) # coordinates weights

    self.fetch += [probs, confs, conid, cooid, proid]
    true = tf.concat([probs, confs, coord], 1)
    wght = tf.concat([proid, conid, cooid], 1)
    print('Building {} loss'.format(m['model']))
    loss = tf.pow(net_out - true, 2)
    loss = tf.multiply(loss, wght)
    loss = tf.reduce_sum(loss, 1)
    self.loss = .5 * tf.reduce_mean(loss)
    tf.summary.scalar('{} loss'.format(m['model']), self.loss)
