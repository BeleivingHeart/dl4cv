from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                help="original image")
ap.add_argument("-o", "--output", required=True,
                help="output directory")
ap.add_argument("-p", "--prefix", default='image',
                help="face video to be loaded")
args = vars(ap.parse_args())


def image_generater(args):
    from keras.preprocessing.image import img_to_array
    from keras.preprocessing.image import ImageDataGenerator
    from keras.preprocessing.image import load_img
    import cv2
    import numpy as np

    image = load_img(args['image'])
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1, height_shift_range=0.1,
                          shear_range=0.2, zoom_range=0.2, horizontal_flip=True, fill_mode='nearest')
    
    print('[INFO] generating images...')
    imageGen = aug.flow(
        image, batch_size=1, save_to_dir=args["output"], save_prefix=args["prefix"], save_format="jpg")
    total = 0
    for img in imageGen:
        total += 1
        if total == 10:
            break

if __name__ == '__main__':
    image_generater(args)
