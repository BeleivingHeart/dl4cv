from nn import MiniVGGNet
from keras.datasets import cifar10
from sklearn.preprocessing import LabelBinarizer
from keras.callbacks import ModelCheckpoint
from keras.optimizers import SGD
import numpy as np
import os
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset end with backward slash")
ap.add_argument("-e", "--epochs", type=int, default=40,
                help="epochs to be trained")
ap.add_argument("-o", "--output", required=True,
                help="output path without backward slash")
args = vars(ap.parse_args())

print("[INFO] loading images...")
# function cifar10.load_data has been modified to accpet a path parameter
((trainX, trainY), (testX, testY)) = cifar10.load_data(args["dataset"])
trainX = trainX.astype("float")/255.0
testX = testX.astype("float")/255.0
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)

print("[INFO] Compiling the model...")
sgd = SGD(lr=0.01, momentum=0.9, decay=0.01 /
          float(args["epochs"]), nesterov=True)
model = MiniVGGNet.build(width=32, height=32, depth=3, classes=10)
model.compile(optimizer=sgd, loss="categorical_crossentropy",
              metrics=["accuracy"])

fname = os.path.sep.join( # If fname is a ﬁle path without any template variables only the single best will be saved, which is prefered
    [args["output"], "weights-{epoch: 03d}-{val_loss: .4f}.h5"])
checkPoint = ModelCheckpoint(
    fname, monitor="val_loss", verbose=1, save_best_only=True)
callbacks = [checkPoint]
print("[INFO] Training the network...")
H = model.fit(trainX, trainY, validation_data=(testX, testY),
              epochs=args["epochs"], batch_size=64, verbose=1, callbacks=callbacks)
