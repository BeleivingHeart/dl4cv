from keras.callbacks import BaseLogger
import matplotlib.pyplot as plt 
import numpy as np 
import json
import os

class TrainingMonitor(BaseLogger):
    def __init__(self, figPath, jsonPath = None, startAt = 0):
        super(TrainingMonitor, self).__init__()   # !!!
        self.figPath = figPath
        self.jsonPath = jsonPath
        self.startAt = startAt
        self.H = {}


    def on_train_start(self, logs = {}):
        if self.jsonPath is not None:
            if os.path.exists(self.jsonPath):
                self.H = json.loads(open(self.jsonPath).read())
                if self.startAt > 0:
                    for k in self.H.keys():
                        self.H[k] = self.H[k][:self.startAt]

    def on_epoch_end(self, epoch, logs = {}):
        for (k, v) in logs.items():
            l = self.H.get(k,[]) # This method allows to return a default value if 
            l.append(v)   # the key is missing. !!! Note that missing != None 
            self.H[k]=l

        if self.jsonPath is not None:
            f = open(self.jsonPath, 'w')
            f.write(json.dumps(self.H))
            f.close()

        if len(self.H['acc']) > 1:
            x_axis = np.arange(0, len(self.H['acc']))
            plt.style.use('ggplot')
            plt.figure()
            plt.plot(x_axis, self.H['loss'], label="train_loss")
            plt.plot(x_axis, self.H['val_loss'], label="val_loss")
            plt.plot(x_axis, self.H['acc'], label="acc")
            plt.plot(x_axis, self.H['val_acc'], label="val_acc")
            plt.title(f"Training Loss and Accuracy [epoch: {len(self.H['acc'])}")
            plt.xlabel("Epoch #")
            plt.ylabel("Loss/Accuracy")
            plt.legend()
            plt.savefig(self.figPath)
            plt.close()
