from argparse import ArgumentParser
from random import shuffle
from keras.applications import VGG16, imagenet_utils
from imageNet.myDataset import load_dataset_paths
import progressbar, os
from imutils import paths
from IO import HDF5DatasetWriter
from sklearn.preprocessing import LabelEncoder
import numpy as np 
from keras.preprocessing.image import load_img, img_to_array

ap = ArgumentParser()
ap.add_argument("-e", "--epochs", type=int, default=40,
                help="epochs to be trained")
ap.add_argument("-b", "--batch_size", type=int, default=32,
                help="batch size for prediction")
ap.add_argument("-s", "--buffer_size", type=int, default=1000,
                help="buffer size for writing to file")
ap.add_argument("-c", "--classes", type=int, default=8,
                help="how many classes you want to predict")
ap.add_argument("-d", "--dataset", help="image dataset")
ap.add_argument("-o", "--output", required=True, help="output .h5 file")
args = vars(ap.parse_args())

# initialize dataset
cs = ["ostrich","robin","magpie","vulture","bullfrog","loggerhead","box_turtle","whiptail","agama","Gila_monster"]
numClasses =args['classes']
imagePaths, classNames = load_dataset_paths(numClasses,args['output'],classes=cs,num_each_class=100)
# imagePaths = list(paths.list_images(args['dataset']))
shuffle(imagePaths)
labels = [p.split(os.path.sep)[-2] for p in imagePaths]
le = LabelEncoder()
labels =le.fit_transform(labels)

print('[INFO] loading the model...')
model = VGG16(include_top=False)

# initialize the output file
dataset = HDF5DatasetWriter((len(imagePaths),512*7*7),args['output']+f'/{numClasses}classes.h5', dataKey='features', bufSize=args['buffer_size'])
dataset.storeClassLabels(le.classes_)

# initialize progress bar
widgets = ["Extracting Features: ", progressbar.Percentage(
), ' ', progressbar.Bar(), " ", progressbar.ETA()]
pBar = progressbar.ProgressBar(max_value=len(imagePaths),widgets=widgets).start()

# extract features
print('[INFO] extracting features...')
bs = args['batch_size']
l = len(imagePaths)
for batch_start in np.arange(0,len(imagePaths),bs):
    pBar.update(batch_start)
    batch_end = batch_start + bs
    batch_paths = imagePaths[batch_start:batch_end]
    if batch_end > l:
        batch_paths = imagePaths[batch_start:]
    batch_images = []
    for path in batch_paths:
        image = load_img(path, target_size=(224,224))
        image = img_to_array(image)
        image = np.expand_dims(image,axis=0)
        image = imagenet_utils.preprocess_input(image)
        batch_images.append(image)
    batch_images = np.vstack(batch_images)
    batch_features = model.predict(batch_images, bs)
    batch_features = batch_features.reshape(len(batch_features),-1)
    batch_labels = labels[batch_start:batch_end]
    if batch_end > l:
        batch_labels = labels[batch_start:]
    dataset.add(batch_features, batch_labels)
    

dataset.close()
pBar.finish()

