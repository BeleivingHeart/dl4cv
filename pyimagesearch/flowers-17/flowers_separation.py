import os
import glob
from shutil import move
from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to the flower dataset")
args = vars(ap.parse_args())

names = ['Daffodil', 'Snowdrop', 'LilyValley', 'Bluebell', 'Crocus', 'Iris', 'Tigerlily', 'Tulip',
         'Fritillary', 'Sunflower', 'Daisy', ' ColtsFoot', 'Dandelion', 'Cowslip', 'Buttercup', 'Windflower', ' Pansy']
os.chdir(args['dataset'])
imagePaths = glob.glob(args['dataset']+'/jpg/*.jpg')

if not imagePaths:
    print('[INFO] imagePaths is empty')
else:
    for i in range(17):
        start = i*80
        end = start + 80
        current_paths = imagePaths[start:end]
        newFolder = names[i]
        if not os.path.exists(newFolder):
            os.mkdir(newFolder)
        for path in current_paths:
            imageName = path.split(os.path.sep)[-1]
            newPath = os.path.sep.join([newFolder,imageName])
            print(f'[INFO] moving {path} to {newPath}...')
            move(path,newPath)