from argparse import ArgumentParser

from keras.callbacks import LearningRateScheduler
from keras.datasets import cifar10
from keras.optimizers import SGD
from keras_preprocessing.image import ImageDataGenerator
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelBinarizer
import numpy as np
import matplotlib
import os, sys
sys.path.append('../pyimagesearch')
from callbacks import TrainingMonitor
from nn.minigooglenet import MiniGoogLeNet

matplotlib.use('Agg')


NUM_EPOCHS = 70
INIT_LR = 5e-3

def poly_decay(epoch):
    max_epoch = NUM_EPOCHS
    init_lr = INIT_LR
    power = 1.0

    alpha = init_lr * (epoch / float(max_epoch)) ** power

    return alpha


ap = ArgumentParser()
ap.add_argument("-m", "--model", required=True, help="hdf5 model to be saved")
ap.add_argument("-o", "--output", required=True, help="output path")
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset")
args = vars(ap.parse_args())

# load in data and preprocess them
print("[INFO] loading images...")
((trainX, trainY), (testX, testY)) = cifar10.load_data(args["dataset"])
trainX = trainX.astype("float") / 255.0
testX = testX.astype("float") / 255.0
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)
train_mean = np.mean(trainX, axis=0)
trainX -= train_mean
testX -= train_mean

# construct the model
print("[INFO] compiling the model...")
opt = SGD(lr=INIT_LR, momentum=0.9)
model = MiniGoogLeNet.build(32, 32, 3, 10)
model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

# fit the model
print("[INFO] fitting the model...")
bs = 32
figPath = os.path.sep.join([args['output'], f'{os.getpid()}.png'])
jsonPath = os.path.sep.join([args['output'], f'{os.getpid()}.json'])
callbacks = [TrainingMonitor(figPath, jsonPath), LearningRateScheduler(poly_decay)]
aug = ImageDataGenerator(width_shift_range=0.1, height_shift_range=0.1, horizontal_flip=True, fill_mode='nearest')
model.fit_generator(aug.flow(trainX, trainY, batch_size=bs), steps_per_epoch=len(trainX) // bs, epochs=NUM_EPOCHS,
                    callbacks=callbacks, validation_data=(testX, testY), verbose=1)

# evaluate the model
print("[INFO] evaluating the model...\n")
preds = model.predict(testX)
print(classification_report(testY.argmax(axis=1), preds.argmax(axis=1)))

# serialize the model
print("[INFO] serializing the model...")
model.save(os.path.sep.join([args['model'], r'GoogLeNet_cifar10.h5']))
