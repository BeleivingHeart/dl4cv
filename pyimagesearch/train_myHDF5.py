import h5py, pickle
from argparse import ArgumentParser
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV

ap = ArgumentParser()
ap.add_argument("-j", "--jobs", type=int, default=-1,
                help="how many threads to use for learning")
ap.add_argument("-d", "--database", required=True, help="directory to the .h5 files")
ap.add_argument("-m", "--model", required=True, help="output .cpickle file")
args = vars(ap.parse_args())

def train_from_hdf5(args):
    # load in the database
    db = h5py.File(args['database'], 'r')
    train_end = int(db['labels'].shape[0]*0.75)

    # initialize the estimator
    param = dict(C = [0.1, 1.0, 10.0, 100.0, 1000.0, 10000.0])
    model = GridSearchCV(LogisticRegression(), param_grid=param, cv=3, scoring='accuracy',n_jobs=args['jobs'])

    # fit the model
    print('[INFO] trainging the model...')
    model.fit(db['feature'][:train_end],db['labels'][:train_end])
    print(f'[INFO] the best param of the model is {model.best_params_}')

    # make predictions
    preds = model.predict(db['feature'][train_end:])
    from sklearn.metrics import classification_report
    print(classification_report(db['labels'][train_end:],preds)+'\n')

    # # serialize the model
    # print('[INFO] saving the model...')
    # with open(args['model'], 'wb') as wf:
    #     pickle.dump(model.best_estimator_,wf)

    # don't forget to close the database
    db.close()

if __name__ == '__main__':
    train_from_hdf5(args)