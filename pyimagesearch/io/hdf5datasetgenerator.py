import numpy as np 
from keras.utils import np_utils
class HDF5DatasetGenerator:
    """return a batch-size image and label list each yeild
    """
    def __init__(self, dbPath, batchSize, preprocessors=None, aug=None, binarize=True, classes=2):
        self.batchSize = batchSize
        self.preprocessors = preprocessors
        self.aug = aug
        self.binarize = binarize
        self.classes = classes
        import h5py
        self.db = h5py.File(dbPath)
        self.numImages = self.db['labels'].shape[0]

    def generator(self, epochs=np.inf):
        epoch_count = 0
        while epoch_count<epochs:
            for batchStart in np.arange(0, self.numImages, self.batchSize):
                batchEnd = batchStart + self.batchSize
                images = self.db['images'][batchStart:batchEnd]
                labels = self.db['labels'][batchStart:batchEnd]
                if self.binarize:
                    labels = np_utils.to_categorical(labels,num_classes=self.classes)
                if self.preprocessors is not None:
                    procImages = []
                    for image in images:
                        for p in self.preprocessors:
                            image = p.preprocess(image)
                        procImages.append(image)
                    images = np.array(procImages)
                if self.aug is not None:
                    (images, labels) = next(self.aug.flow(images,labels,batch_size=self.batchSize))
                yield (images, labels)
            epoch_count += 1

    def close(self):
        self.db.close()
