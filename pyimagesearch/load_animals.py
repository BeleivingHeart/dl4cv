from preprocessing import ImageToArrayPreprocessor
from preprocessing import SimplePreprocessor
from keras.models import load_model
from datasets import SimpleDatasetLoader
from imutils import paths
import numpy as np
import argparse
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True, help="path to input dataset")
ap.add_argument("-m", "--model", required=True, help="model to be loaded")
args = vars(ap.parse_args())
print("[INFO] loading images...")
imagePaths = np.array(list(paths.list_images(args["dataset"])))
idxs = np.random.randint(0, len(imagePaths), size=(10,))
imagePaths = imagePaths[idxs]

# config the dataloader
sp = SimplePreprocessor(32, 32)
iap = ImageToArrayPreprocessor()
sdl = SimpleDatasetLoader(preprocessors=[sp, iap])
sdl1 = SimpleDatasetLoader()
(data, labels) = sdl.load(imagePaths, verbose=5)
(images, labels) = sdl1.load(imagePaths, verbose=5)
data = data.astype("float")/255.0

# load the model
model = load_model(args["model"])
names = ["cat", "dog", "panda"]

# predict
prediction_indices = model.predict(data).argmax(axis=1)

for image, index in zip(images, prediction_indices):
    prediction = names[index]
    cv2.putText(image, f"label: {prediction}",
                (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
    cv2.imshow("Image", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
