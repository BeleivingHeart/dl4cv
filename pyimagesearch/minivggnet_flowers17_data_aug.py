from minivggnet import MiniVGGNet
from imutils import paths
from sklearn.preprocessing import LabelBinarizer
from keras.callbacks import ModelCheckpoint
from preprocessing import ImageToArrayPreprocessor
from preprocessing import AspectAwarePreprocessor
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
from datasets import SimpleDatasetLoader
from keras.optimizers import SGD
import numpy as np
import os
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset")
ap.add_argument("-e", "--epochs", type=int, default=100,
                help="epochs to be trained")
ap.add_argument("-o", "--output", required=True,
                help="output path")
args = vars(ap.parse_args())

# grab the list of images
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))
# the length of the first classNames is very huge because of overlapping
classNames = [path.split(os.path.sep)[-2] for path in imagePaths]
classNames = [name for name in np.unique(classNames)]
p1 = AspectAwarePreprocessor(64, 64)
p2 = ImageToArrayPreprocessor()
sdl = SimpleDatasetLoader(preprocessors=[p1, p2])
data, labels = sdl.load(imagePaths)

# preprocess the data and labels
data = data/255.0
lb = LabelBinarizer()
labels = lb.fit_transform(labels)
(trainX, testX, trainY, testY) = train_test_split(
    data, labels, test_size=0.25, random_state=42)

print("[INFO] Compiling the model...")
sgd = SGD(lr=0.01, momentum=0.9, decay=0.01 /
          float(args["epochs"]), nesterov=True)
model = MiniVGGNet.build(width=64, height=64, depth=3, classes=len(classNames))
model.compile(optimizer=sgd, loss="categorical_crossentropy",
              metrics=["accuracy"])

# construct callbacks
fname = os.path.sep.join(  # If fname is a ﬁle path without any template variables only the single best will be saved, which is prefered
    [args["output"], "minivggnet_flowers17_with_augmentation.h5"])
checkPoint = ModelCheckpoint(
    fname, monitor="val_loss", verbose=1, save_best_only=True)
callbacks = [checkPoint]

# fit the model
print("[INFO] Training the network...")
aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1, height_shift_range=0.1,
                         shear_range=0.2, zoom_range=0.2, horizontal_flip=True, fill_mode='nearest')
H = model.fit_generator(aug.flow(trainX, trainY, batch_size=32), validation_data=(testX, testY),
                        epochs=args["epochs"], verbose=1, callbacks=callbacks, steps_per_epoch=len(trainX)//32)

# evaluate the model
print("[INFO] Evaluating the model...")
predictions = model.predict(testX, batch_size=32)
print(classification_report(testY.argmax(axis=1), predictions.argmax(
    axis=1), target_names=classNames))

plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, args["epochs"]), H.history["val_acc"], label="val_acc")
plt.title("Training loss and accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig(args["output"]+"\minivggnet_flowers17_with_augmentation.png")
plt.show()
