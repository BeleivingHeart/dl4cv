from argparse import ArgumentParser
ap = ArgumentParser()
ap.add_argument("-i", "--images", type=str, required=True,
                help="path to the flowers17 dataset")
ap.add_argument("-m", "--model", type=str, required=True,
                help="path to the output model")
ap.add_argument("-b", "--batch_size", type=int, default=8,
                help="batch size for training")
args = vars(ap.parse_args())

# building base model
print('[INFO] loading vgg16...')
from keras.applications import VGG16
from keras.models import Input
baseModel = VGG16(include_top=False, input_tensor=Input(shape=(224,224,3)))

# building complete model
print('[INFO] building complete model...')
from nn.fcheadnet import FCHeadNet
headModel = FCHeadNet.build(baseModel,17,256)
from keras.models import Model
model = Model(inputs=baseModel.input, outputs=headModel)

# load in data
print('[INFO] loading flowers17 dataset...')
from imutils import paths
import os
imagePaths = list(paths.list_images(args['images']))
classNames = os.listdir(args['images'])
from preprocessing import AspectAwarePreprocessor
from preprocessing import ImageToArrayPreprocessor
from datasets import SimpleDatasetLoader
aap = AspectAwarePreprocessor(224,224)
iap = ImageToArrayPreprocessor()
sdl = SimpleDatasetLoader(preprocessors=[aap, iap])
data, labels = sdl.load(imagePaths, verbose=500)
data = data / 255.0

# preprocess data
from sklearn.model_selection import train_test_split
(trainX, testX, trainY, testY) = train_test_split(data, labels,test_size=0.25,random_state=42)
from sklearn.preprocessing import LabelBinarizer
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.fit_transform(testY)

# set up data augmentation
from keras.preprocessing.image import ImageDataGenerator
aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1, height_shift_range=0.1,
                         shear_range=0.2, zoom_range=0.2, horizontal_flip=True, fill_mode='nearest')

# complie the model first stage
print('[INFO] compiling the model for warming up...')
# freeze the base model
for layer in baseModel.layers:
    layer.trainable = False
from keras.optimizers import RMSprop
optimizer = RMSprop(lr = 0.001)
model.compile(optimizer=optimizer,loss='categorical_crossentropy',metrics=['accuracy'])

# fit the model for warming up
print('[INFO] training the model for warming up...')
model.fit_generator(aug.flow(trainX, trainY, batch_size=args['batch_size']), validation_data=(testX, testY),
                        epochs=25, verbose=1, steps_per_epoch=len(trainX)//args['batch_size'])

# evaluate the model first stage
print('[INFO] evaluating first stage...')
preds = model.predict(testX)
from sklearn.metrics import classification_report
print(classification_report(testY.argmax(axis=1), preds.argmax(
    axis=1), target_names=classNames))

# unfreeze part of the base model
for layer in baseModel.layers[15:]:
    layer.trainable = True

# complie model for fine tuning
print('[INFO] compiling the model for fine tuning...')
from keras.optimizers import SGD
optimizer = SGD(lr=0.001)
model.compile(optimizer=optimizer,loss='categorical_crossentropy',metrics=['accuracy'])

# fine tune the model
print('[INFO] fine-tuning the model...')
model.fit_generator(aug.flow(trainX, trainY, batch_size=args['batch_size']), validation_data=(testX, testY),
                        epochs=100, verbose=1, steps_per_epoch=len(trainX)//args['batch_size'])

# evaluate the fine-tuned model
print('[INFO] evaluating fine-tuned model...')
preds = model.predict(testX)
print(classification_report(testY.argmax(axis=1), preds.argmax(
    axis=1), target_names=classNames))

# save the model
print('[INFO] serializing fine-tuned model...')
model.save(args['model'])