from UTILS.ranked import rank5_accuracy, print_ranks
import h5py, pickle
from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-d", "--database", required=True, help="directory to the .h5 files")
ap.add_argument("-m", "--model", required=True, help="input .cpickle file")
args = vars(ap.parse_args())

# load the model and the dataset
with open(args['model'], 'rb') as rf:
    model = pickle.load(rf)
db = h5py.File(args['database'], 'r')

# make predictions
print('[INFO] predicting...')
test_start = int(db['labels'].shape[0] * 0.75)
preds = model.predict_proba(db['feature'][test_start:])  
labels = db['labels'][test_start:]
(rank1, rank5) = rank5_accuracy(preds, labels)
print_ranks(rank1, rank5)
db.close()