from minivggnet import MiniVGGNet
import matplotlib
matplotlib.use("Agg")
from keras.datasets import cifar10
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelBinarizer
from keras.optimizers import SGD
from keras.callbacks import LearningRateScheduler
import matplotlib.pyplot as plt
import numpy as np
import argparse

# Define a learning rate schedualer that accepts epoch and return learning rate
def step_decay(epoch):
    initAlpha = 0.01
    decayEvery = 5.0
    decayRate = 0.5
    alpha = initAlpha * decayRate ** np.floor((epoch+1)/decayEvery)
    return alpha

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset")
ap.add_argument("-e", "--epochs", type=int, default=40,
                help="epochs to be trained")
ap.add_argument("-o", "--output", required=True, help="output path end with backward slash")
args = vars(ap.parse_args())

print("[INFO] loading images...")
# function cifar10.load_data has been modified to accpet a path parameter
((trainX, trainY), (testX, testY)) = cifar10.load_data(args["dataset"])
trainX = trainX.astype("float")/255.0
testX = testX.astype("float")/255.0
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)

print("[INFO] Compiling the model...")
sgd = SGD(lr=0.01, momentum=0.9, decay=0.01/float(args["epochs"]), nesterov=True)
model = MiniVGGNet.build(width=32, height=32, depth=3, classes=10)
model.compile(optimizer=sgd, loss="categorical_crossentropy",
              metrics=["accuracy"])

# callbacks = [LearningRateScheduler(step_decay)]       /// Either use this callback or just linear dacay of learning rate strategy     

print("[INFO] Training the network...")
# H = model.fit(trainX, trainY, validation_data=(testX, testY),
#               epochs=args["epochs"], batch_size=64, verbose=1, callbacks=callbacks)
H = model.fit(trainX, trainY, validation_data=(testX, testY),
              epochs=args["epochs"], batch_size=64, verbose=1)

print("[INFO] Evaluating the model...")
predictions = model.predict(testX, batch_size=64)
labelNames = ["airplane", "automobile", "bird", "cat",
              "deer", "dog", "frog", "horse", "ship", "truck"]
print(classification_report(testY.argmax(axis=1), predictions.argmax(
    axis=1), target_names=labelNames))
model.save(args["output"]+'\minivggnet_cifar10.h5')

plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, args["epochs"]), H.history["val_acc"], label="val_acc")
plt.title("Training loss and accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig(args["output"]+"\minivggnet_cifar10.png")
plt.show()

