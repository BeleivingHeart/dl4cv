from nn import MiniVGGNet
from keras.datasets import cifar10
from sklearn.preprocessing import LabelBinarizer
from keras.callbacks import ModelCheckpoint
from keras.optimizers import SGD
import numpy as np
import os
import argparse
import matplotlib.pyplot as plt

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset end with backward slash")
ap.add_argument("-e", "--epochs", type=int, default=40,
                help="epochs to be trained")
ap.add_argument("-n", "--num_models", type=int, default=5,
                help="numbers of model to be generated")
ap.add_argument("-o", "--output", required=True,
                help="output path without backward slash")
args = vars(ap.parse_args())

print("[INFO] loading images...")
# function cifar10.load_data has been modified to accpet a path parameter
((trainX, trainY), (testX, testY)) = cifar10.load_data(args["dataset"])
trainX = trainX.astype("float")/255.0
testX = testX.astype("float")/255.0
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)
labelNames = ["airplane", "automobile", "bird", "cat",
              "deer", "dog", "frog", "horse", "ship", "truck"]

# generate models
for i in range(args['num_models']):
    print(f"[INFO] generating model: {i+1}/{args['num_models']}...")
    sgd = SGD(lr=0.01, momentum=0.9, decay=0.01 /
            float(args["epochs"]), nesterov=True)
    model = MiniVGGNet.build(width=32, height=32, depth=3, classes=10)
    model.compile(optimizer=sgd, loss="categorical_crossentropy",
                metrics=["accuracy"])

    fname = os.path.sep.join( # If fname is a ﬁle path without any template variables only the single best will be saved, which is prefered
        [args["output"], f"ensemble_{str(i+1).zfill(3)}.h5"])
    checkPoint = ModelCheckpoint(
        fname, monitor="val_loss", verbose=1, save_best_only=True)
    callbacks = [checkPoint]
    H = model.fit(trainX, trainY, validation_data=(testX, testY),
                epochs=args["epochs"], batch_size=64, verbose=1, callbacks=callbacks)
    
    # evaluate model
    plt.style.use("ggplot")
    plt.figure()
    plt.plot(np.arange(0, args["epochs"]), H.history["loss"], label="train_loss")
    plt.plot(np.arange(0, args["epochs"]), H.history["val_loss"], label="val_loss")
    plt.plot(np.arange(0, args["epochs"]), H.history["acc"], label="train_acc")
    plt.plot(np.arange(0, args["epochs"]), H.history["val_acc"], label="val_acc")
    plt.title("Training loss and accuracy")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss/Accuracy")
    plt.legend()
    path = os.path.sep.join([args['output'],f'performance_{str(i+1).zfill(3)}.png'])
    plt.savefig(path)