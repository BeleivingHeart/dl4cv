import numpy as np
import cv2


class CropPreprocessor:
    """Sample a numpy list of fixed-size subregions of an image
    """

    def __init__(self, width, height, horiz=True, inter=cv2.INTER_AREA):
        self.width = width
        self.height = height
        self.horiz = horiz
        self.inter = inter

    def preprocess(self, image):
        (h, w) = image.shape[:2]
        crops = []
        rects = [
            [0, 0, self.width, self.height],
            [0, h-self.height, self.width, h],
            [w-self.width, 0, w, self.height],
            [w-self.width, h-self.height, w, h]
        ]
        dw = int((w-self.width)/2)
        dh = int((h-self.height)/2)

        rects.append([dw,dh,self.width+dw,self.height+dh])
        for (xmin,ymin,xmax,ymax) in rects:
            crop = image[ymin:ymax, xmin:xmax]
            crop = cv2.resize(crop,(self.width, self.height),interpolation=self.inter)
            crops.append(crop)

        if self.horiz:
            mirrors = [cv2.flip(c,1) for c in crops]
            crops.extend(mirrors)

        return np.array(crops)
        

