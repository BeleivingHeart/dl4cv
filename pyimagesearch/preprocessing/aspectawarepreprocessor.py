from imutils import resize
import cv2


class AspectAwarePreprocessor:
    """ Accept and output cv2 style numpy array
    """
    def __init__(self, width, height, inter=cv2.INTER_AREA):
        self.width = width
        self.height = height
        self.inter = inter

    def preprocess(self, image):
        (h, w) = image.shape[:2]
        dh = 0
        dw = 0
        if h > w:
            image = resize(image, width=self.width, inter=self.inter)
            dh = (image.shape[0]-self.height)//2
        else:
            image = resize(image, height=self.height, inter=self.inter)
            dw = (image.shape[1] - self.width)//2

        image = image[dh:image.shape[0]-dh, dw:image.shape[1]-dw]
        return cv2.resize(image, (self.width, self.height), interpolation=self.inter)
