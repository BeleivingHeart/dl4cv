from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Dense
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras import backend as K 

class LetNet:
    """ LetNet model that is composed of (conv -> pooling)*2 
        -> dense -> dense ->softmax
    """
    def build(height, width, depth, classes):
        inputShape = (height, width, depth)
        if K.image_data_format() == "channels_first":
            inputShape = (depth, height, width )
        model = Sequential()
        model.add(Conv2D(20,(5,5),input_shape=inputShape,padding="same"))
        model.add(Activation("relu"))
        model.add(MaxPooling2D(strides=(2,2)))
        model.add(Conv2D(50,(5,5),padding="same"))
        model.add(Activation("relu"))
        model.add(MaxPooling2D(strides=(2,2)))
        model.add(Flatten())
        model.add(Dense(500))
        model.add(Activation("relu"))
        model.add(Dense(classes))
        model.add(Activation("softmax"))
        return model





