from .lenet import LetNet
from .minivggnet import MiniVGGNet
from .shallownet import ShallowNet