from keras import Input, Model
from keras.layers import Conv2D, BatchNormalization, Activation, concatenate, MaxPooling2D, K, AveragePooling2D, \
    Dropout, Flatten, Dense


class MiniGoogLeNet:
    def conv_module(x, K, kX, kY, stride, chanDim, padding = 'same'):
        """
        :param x: previous layer
        :param K: the number of filters our CONV layer is going to learn
        :param kX: filter width
        :param kY: filter height
        :param stride: filter stride
        :param chanDim: the axis where channel lies
        :param padding: padding strategy
        :return: a micro module
        """
        # CONV => BN => RELU
        x = Conv2D(K, (kX, kY), strides=stride, padding=padding)(x)
        x = Activation('relu')(x)
        x = BatchNormalization(axis=chanDim)(x)

        return x

    def inception_module(x, numK1x1, numK3x3, chanDim):
        """
        :param x: previous layer
        :param numK1x1: number of 1x1 filters
        :param numK3x3: number of 3x3 filters
        :param chanDim: the axis where channel lies
        :return: concatenate(conv_1x1, conv_3x3)
        """
        conv_1x1 = MiniGoogLeNet.conv_module(x, numK1x1, 1, 1, (1,1), chanDim)
        conv_3x3 = MiniGoogLeNet.conv_module(x, numK3x3, 3, 3, (1,1), chanDim)
        x = concatenate(inputs=[conv_1x1, conv_3x3], axis=chanDim)

        return x

    def downsample_module(x, K, chanDim):
        """
        :param x: previous layer
        :param K: number of 3x3 filters that used to downsample
        :param chanDim: the axis where channel lies
        :return: concatenate(conv_3x3, pool)
        """
        conv_3x3 = MiniGoogLeNet.conv_module(x, K, 3, 3, (2,2), chanDim, padding='valid')
        pool = MaxPooling2D((3,3), strides=(2,2))(x)
        x = concatenate([conv_3x3, pool], axis=chanDim)

        return x

    def build(width, height, depth, classes):
        inputShape = (height, width, depth)
        chanDim = -1

        if K.image_data_format() == 'channels_first':
            inputShape = (depth, height, width)
            chanDim = 1
        inputs = Input(shape=inputShape)

        x = MiniGoogLeNet.conv_module(inputs, 96, 3, 3, stride=(1,1), chanDim=chanDim)
        # two inception modules followed by a downsample module
        x = MiniGoogLeNet.inception_module(x, 32, 32, chanDim)
        x = MiniGoogLeNet.inception_module(x, 32, 48, chanDim)
        x = MiniGoogLeNet.downsample_module(x,80,chanDim)
        # four inception modules followed by a downsample module
        x = MiniGoogLeNet.inception_module(x, 112, 48, chanDim)
        x = MiniGoogLeNet.inception_module(x, 96, 64, chanDim)
        x = MiniGoogLeNet.inception_module(x, 80, 80, chanDim)
        x = MiniGoogLeNet.inception_module(x, 48, 96, chanDim)
        x = MiniGoogLeNet.downsample_module(x, 96, chanDim)
        #  two inception modules followed by a global pool and dropout
        x = MiniGoogLeNet.inception_module(x, 176, 160, chanDim)
        x = MiniGoogLeNet.inception_module(x, 176, 160, chanDim)
        x = AveragePooling2D((7,7))(x)
        x = Dropout(0.5)(x)
        # softmax classifier
        x = Flatten()(x)
        x = Dense(classes)(x)
        x = Activation('softmax')(x)

        # create the model
        model = Model(inputs, x, name='googlenet')

        # return the constructed network architecture
        return model