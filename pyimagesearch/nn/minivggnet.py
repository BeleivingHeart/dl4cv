from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Flatten
from keras.layers.core import Activation
from keras.layers.core import Dense
from keras.layers.core import Dropout
from keras import backend as K 

class MiniVGGNet:
    def build(height, width, depth, classes):
        model = Sequential()
        inputShape = (height, width, depth)
        axis_of_channel = -1
        if(K.image_data_format() == "channels_first"):
            inputShape = (depth, height, width)
            axis_of_channel = 1 # 0 axis is the axis of images
        
        # Adding the first stack
        model.add(Conv2D(32,(3,3),input_shape = inputShape, padding = "same"))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=axis_of_channel))
        model.add(Conv2D(32,(3,3), padding = "same"))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=axis_of_channel))
        model.add(MaxPooling2D(strides=(2,2)))
        model.add(Dropout(0.25))

        # Adding the second very alike stack
        model.add(Conv2D(64,(3,3), padding = "same"))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=axis_of_channel))
        model.add(Conv2D(64,(3,3), padding = "same"))
        model.add(Activation('relu'))
        model.add(BatchNormalization(axis=axis_of_channel))
        model.add(MaxPooling2D(strides=(2,2)))
        model.add(Dropout(0.25))

        # Adding fc part
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(BatchNormalization()) # Now that the channels are merged so we 
        model.add(Dropout(0.5))         # nomalize along the 0 axis
        model.add(Dense(classes))
        model.add(Activation('softmax'))

        return model
