import argparse
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset end with backward slash")
ap.add_argument("-m", "--models", required=True,
                help="directory containing the classifier ensembles")
ap.add_argument("-o", "--output", required=True, help="output path")
args = vars(ap.parse_args())

# load data
print("[INFO] loading images...")
from keras.datasets import cifar10
# function cifar10.load_data has been modified to accpet a path parameter
((trainX, trainY), (testX, testY)) = cifar10.load_data(args["dataset"])
testX = testX.astype("float")/255.0
trainX = []
trainY = []
from sklearn.preprocessing import LabelBinarizer
lb = LabelBinarizer()
testY = lb.fit_transform(testY)
labelNames = ["airplane", "automobile", "bird", "cat",
              "deer", "dog", "frog", "horse", "ship", "truck"]

# load models
from glob import glob
import os
from keras.models import load_model
modelPaths = glob(os.path.sep.join([args['models'], '*.h5']))
predictions = []
for path in modelPaths:
    model = load_model(path)
    preds = model.predict(testX, batch_size=64)
    predictions.append(preds)

# evaluate individuals
print('[INFO] evaluating individual models...')
from sklearn.metrics import classification_report
for i, preds in enumerate(predictions):
    print(f'    [INFO] evaluating model: {i+1}/{len(modelPaths)}...')
    report = classification_report(testY.argmax(axis=1), preds.argmax(
        axis=1), target_names=labelNames)
    reportPath = os.path.sep.join([args['output'],f"individual_report_{str(i+1).zfill(3)}.txt"])
    with open(reportPath, 'w') as wf:
        wf.write(report)

# evaluate ensemble
print('[INFO] evaluating ensemble model...')
import numpy as np 
predictions = np.array(predictions).mean(axis=0)
report = classification_report(testY.argmax(axis=1), predictions.argmax(
        axis=1), target_names=labelNames)
reportPath = os.path.sep.join([args['output'],f"ensemble_report.txt"])
with open(reportPath, 'w') as wf:
    wf.write(report)