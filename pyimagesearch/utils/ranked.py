import numpy as np


def rank5_accuracy(preds, labels):
    """return rank1 and rand5 accuracy within (0,1)
    """
    rank1 = 0
    rank5 = 0
    for p, l in zip(preds, labels):
        p = np.argsort(p)[::-1]
        if l in p[:5]:
            rank5 += 1
            if l == p[0]:
                rank1 += 1
    rank1 /= float(len(labels))
    rank5 /= float(len(labels))
    return (rank1, rank5)

def print_ranks(rank1, rank5):
    """print out rank1 and rank5 in percentage
    """
    print('[REPORT] rank1 accuracy: {:.2f}%'.format(rank1*100))
    print('[REPORT] rank5 accuracy: {:.2f}%'.format(rank5*100))
