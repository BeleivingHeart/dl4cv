import imutils
import cv2

class CaptchaHelper:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def preprocess(self, image):
        (h, w) = image.shape[:2]
        if h>=w:
            image = imutils.resize(image,height=self.height)
            padW = int((self.width - w)/2.0)
            image = cv2.copyMakeBorder(image, 0, 0, padW, padW, cv2.BORDER_REPLICATE)
        else:
            image = imutils.resize(image,width= self.width)
            padH = int((self.height - h)/2.0)
            image = cv2.copyMakeBorder(image, padH, padH, 0, 0, cv2.BORDER_REPLICATE)
        image = cv2.resize(image,(self.height,self.width))
        return image
    


def preprocess(image, width, height):
    (h, w) = image.shape[:2]
    if h>=w:
        image = imutils.resize(image,height=height)
        padW = int((width - w)/2.0)
        image = cv2.copyMakeBorder(image, 0, 0, padW, padW, cv2.BORDER_REPLICATE)
    else:
        image = imutils.resize(image,width= width)
        padH = int((height - h)/2.0)
        image = cv2.copyMakeBorder(image, padH, padH, 0, 0, cv2.BORDER_REPLICATE)
    image = cv2.resize(image,(height,width))
    return image
    
   