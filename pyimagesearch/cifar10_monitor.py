import matplotlib
matplotlib.use('Agg')
from callbacks import TrainingMonitor
from sklearn.preprocessing import LabelBinarizer
from minivggnet import MiniVGGNet
from keras.datasets import cifar10
from keras.optimizers import SGD
from keras.callbacks import LearningRateScheduler
import numpy as np
import os
import argparse

def step_decay(epoch):
    initAlpha = 0.01
    decayEvery = 5.0
    decayRate = 0.5
    alpha = initAlpha * decayRate ** np.floor((epoch+1)/decayEvery)
    return alpha

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset")
ap.add_argument("-e", "--epochs", type=int, default=40,
                help="epochs to be trained")
ap.add_argument("-o", "--output", required=True, help="output path end with backward slash")
args = vars(ap.parse_args())

print(f"[INFO] process ID: {os.getpid()}")

print("[INFO] loading images...")
# function cifar10.load_data has been modified to accpet a path parameter
((trainX, trainY), (testX, testY)) = cifar10.load_data(args["dataset"])
trainX = trainX.astype("float")/255.0
testX = testX.astype("float")/255.0
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)

print("[INFO] Compiling the model...")
sgd = SGD(lr=0.01, momentum=0.9, nesterov=True)   # use only decay param or only scheduler
model = MiniVGGNet.build(width=32, height=32, depth=3, classes=10)
model.compile(optimizer=sgd, loss="categorical_crossentropy",
              metrics=["accuracy"])

figPath = os.path.sep.join([args["output"], f"{os.getpid()}.png"])
jsonPath = os.path.sep.join([args["output"], f"{os.getpid()}.json"])

callbacks = [LearningRateScheduler(step_decay),TrainingMonitor(figPath, jsonPath)]       

print("[INFO] Training the network...")
# H = model.fit(trainX, trainY, validation_data=(testX, testY),
#               epochs=args["epochs"], batch_size=64, verbose=1, callbacks=callbacks)
H = model.fit(trainX, trainY, validation_data=(testX, testY),
              epochs=args["epochs"], batch_size=64, verbose=1, callbacks=callbacks)
