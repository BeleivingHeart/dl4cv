import os, cv2, json, progressbar
from xml.etree import ElementTree as et 

from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-d", "--directory", required=True, help="directory to data folders")
args = vars(ap.parse_args())

with open('list_for_extraction.json', 'r') as rf:
    data = json.load(rf)
    keys = list(data.keys())
os.chdir(args['directory'])

# check if any folder contains extra folder
for key in keys:
    n = len(os.listdir(key))
    if n !=2:
        exit(f'{key} has {n} folders inside!')
print('[INFO] No extra folder found...')

# check if any subfolder inappropriately extracted or empty
for key in keys:
    n1 = len(os.listdir(f'{key}/Annotation'))
    n2 = len(os.listdir(f'{key}/images'))
    if n1<100:
        exit(f'{key}/Annotation has less than 100 objects')
    if n2<100:
        exit(f'{key}/images has less than 100 objects')
    print(f'[INFO] checking folder: {key}')
print('[INFO] All subfolder has more that 100 objects...')

# initialize progress bar
widgets = ["Extracting ROIs: ", progressbar.Percentage(
), ' ', progressbar.Bar(), " ", progressbar.ETA()]
pBar = progressbar.ProgressBar(max_value=len(keys),widgets=widgets).start()

# extract roi images
print('[INFO] Extracting ROIs...')
for j,key in enumerate(keys):
    # extract only images that have roi infomations
    pBar.update(j)
    xmlList = os.listdir(f'{key}/Annotation')
    imageList = os.listdir(f'{key}/images')
    imageHashList = []
    xmlHashList = []
    for path in imageList:
        h = path.split('.')[0]
        imageHashList.append(h)
    for path in xmlList:
        h = path.split('.')[0]
        xmlHashList.append(h)

    extractionList = []
    for h in imageHashList:
        for x in xmlHashList:
            if h == x:
                extractionList.append(h)
                break
    # rearrage xmlList and imageList to be all valid
    imageHashList = []
    xmlHashList = []
    xmlList =[]
    imageList= []
    for h in extractionList:
        x = h + '.xml'
        i = h + '.JPEG'
        xmlList.append(x)
        imageList.append(i)
    
    # begin roi extraction
    for x,i,h in zip(xmlList,imageList,extractionList):
        xmlPath = os.path.sep.join([key,'Annotation',x])
        imagePath = os.path.sep.join([key,'images',i])
        with open(xmlPath, 'r') as rf:
            tree = et.parse(rf)
            root = tree.getroot()
            objects = root.findall('object')
            bndboxs = []
            for o in objects:
                bndbox = o.find('bndbox')
                children = bndbox.getchildren()
                roi = {}
                for child in children:
                    roi[child.tag] = int(child.text)
                bndboxs.append(roi)

        image = cv2.imread(imagePath)
        image_splits = []
        for roi in bndboxs:
            xmin = roi['xmin']
            ymin = roi['ymin']
            xmax = roi['xmax']
            ymax = roi['ymax']
            segment = image[ymin:ymax,xmin:xmax]
            image_splits.append(segment)
        for index,img in enumerate(image_splits):
            index = h + '_' + str(index) + '.JPEG'
            outPath = os.path.sep.join([key,index])
            cv2.imwrite(outPath,img)
pBar.finish()