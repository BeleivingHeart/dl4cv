# end with n02006656
import json, os, glob, webbrowser, time, progressbar
from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-a", "--archives", required=True, help="path to input archives")
args = vars(ap.parse_args())

prefix = 'http://image-net.org/api/download/imagenet.bbox.synset?wnid='

# extract all of the hashes and names from json
hashs = []
names = []
with open('E:\OneDrive\local-git\dl4cv\imageNet\imagenet_class_index.json') as f:
    data = json.load(f)
    for key in data.keys():
        hashs.append(data[key][0])
        names.append(data[key][1])

# save hashes and names only when i have corresponing images
fileList = glob.glob(args['archives'] + '/*.tar')
hashList = []
nameList = []
for f in fileList:
    for h,n in zip(hashs,names):
        if h in f:
            hashList.append(h)
            nameList.append(n)
            break


# 临时！！！
# hashList = []
# with open('E:\OneDrive\local-git\dl4cv\imageNet\list_for_extraction.json','r') as rf:
#     data = json.load(rf)
#     keys =list(data.keys())
#     for key in keys:
#         h = data[key]['hash']
#         hashList.append(h)



urls = []
for h in hashList:
    url = prefix + h
    urls.append(url)

# initialize progress bar
widgets = ["Downloading files: ", progressbar.Percentage(
), ' ', progressbar.Bar(), " ", progressbar.ETA()]
pBar = progressbar.ProgressBar(max_value=len(urls),widgets=widgets).start()


# download begins. this is a special downloading approach we won't use often
print('[INFO] downloading .gz files')
for i,url in enumerate(urls):
    try:
        webbrowser.open(url)
    except:
        print('[ERROR] error downloading zip')
    time.sleep(1)
    pBar.update(i)
pBar.finish()

# write the extraction infomation to json
d = dict()
for n,h in zip(nameList, hashList):
    d[n] = {
        'hash':h,
        'images':h+'.tar',
        'bounding_rects':h+'.tar.gz'
    }
with open(r'list_for_extraction.json','w') as wf:
    json.dump(d, wf, indent=2)

