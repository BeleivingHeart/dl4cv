import os, glob, json
import numpy as np

basePath = "F:\imageNetDataset"

def check_nums(keys, num):
    for key in keys:
        classPaths = glob.glob(os.path.sep.join([basePath,key,'*.JPEG']))
        if len(classPaths)<num:
            return False
    return True


def load_dataset_paths(num, logPath, num_each_class = -1,classes=None):
    """return (imagePaths, classNames) from my customized image dataset
    
    num
        Load num classes of training images from my customized image dataset.
    logPath
        output txt containing all the names fo the classes you use to this path
    num_each_class
        if not left to default, would make sure that each class drawn from the
        dataset would have num_each_class images by pooling only from satisfied 
        classes
    classes    
        You can specify which classes to load by providing a list to classes.
    """


    with open('E:\OneDrive\local-git\dl4cv\pyimagesearch\imageNet\list_for_extraction.json','r') as rf:
        data = json.load(rf)
        keys = list(data.keys())


    if classes is not None:
        # check if classes matches size and within range
        if len(classes) != num:
            exit(f'[ERROR] length of classes should match num')
        count = 0
        for c in classes:
            for i,key in enumerate(keys):
                if c == key:
                    count+=1
                    break
                elif i == len(keys)-1 and c != key:
                    exit(f'[ERROR] {c} not in my classes')
        # pass all check
        load_classes = np.array(classes)
    else:
        load_classes = np.random.choice(keys,num,replace=False)
    print(f'[INFO] loaded paths: {load_classes} from my dataset...')

    # if num_each_class is provided
    if num_each_class > 0:
        while 1:
            if check_nums(load_classes, num_each_class):
                break
            else:
                load_classes = np.random.choice(keys,num,replace=False)
                print('[INFO] repool datasets...')

    allPaths = []
    for c in load_classes:
        classPaths = glob.glob(os.path.sep.join([basePath,c,'*.JPEG']))
        if num_each_class>0:
            classPaths = classPaths[:num_each_class]
        allPaths.extend(classPaths)
    
    # output the class record
    outLog = f'{logPath}/classes_information_{num}.txt'
    with open(outLog,'w') as wf:
        for c in load_classes:
            wf.write(c+'\n')

    return (allPaths, load_classes)








if __name__ == '__main__':
    imagePaths = load_dataset_paths(20,'./')        
    print(len(imagePaths))