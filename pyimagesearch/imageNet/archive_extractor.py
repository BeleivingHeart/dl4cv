import patoolib, os, json, glob, shutil, progressbar
from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-a", "--archives", required=True, help="path to input archives")
args = vars(ap.parse_args())



with open(r'list_for_extraction.json','r') as rf:
    data = json.load(rf)
    keys = list(data.keys())

    # initialize progress bar
    widgets = ["Extracting files: ", progressbar.Percentage(
    ), ' ', progressbar.Bar(), " ", progressbar.ETA()]
    pBar = progressbar.ProgressBar(max_value=len(keys),widgets=widgets).start()

    os.chdir(args['archives'])
    # check if any file is missing
    for key in keys:
        gz = data[key]["bounding_rects"]
        if not os.path.exists(gz):
            exit(f'[ERROR] {gz} is missing!!!')


    for i,key in enumerate(keys):
        if not os.path.exists(key):
            os.mkdir(key)

        tar = data[key]["images"]
        gz = data[key]["bounding_rects"]

        # extract files
        patoolib.extract_archive(tar, outdir=key)
        patoolib.extract_archive(gz, outdir=key)

        # rename folder
        imagesFolder_p = key + '/' + data[key]["hash"]
        imagesFolder_c = key + '/images'
        os.rename(imagesFolder_p,imagesFolder_c)

        # move xml files
        xmls = glob.glob(f'{key}/Annotation/{data[key]["hash"]}/*.xml')
        for xml in xmls:
            name = xml.split(os.path.sep)[-1]
            prefix = xml.split(os.path.sep)[:-1]
            prefix = prefix[0].split('/')[:-1]
            prefix.append(name)
            newPath = os.path.sep.join(prefix)
            shutil.move(xml,newPath)
        shutil.rmtree(f'{key}/Annotation/{data[key]["hash"]}')
        pBar.update(i)
    pBar.finish()

