from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from preprocessing import ImageToArrayPreprocessor
from preprocessing import SimplePreprocessor
from datasets import SimpleDatasetLoader
from shallownet import ShallowNet
from keras.optimizers import SGD
from imutils import paths
import numpy as np
import matplotlib.pyplot as plt
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True, help="path to input dataset")
ap.add_argument("-o", "--output", required=True, help="output hdf5 file")
ap.add_argument("-e", "--epochs",  type = int, default=100,help="training epochs")
args = vars(ap.parse_args())
print("[INFO] loading images...")
imagePaths = list(paths.list_images(args["dataset"]))

# config the dataloader
sp = SimplePreprocessor(32, 32)
iap = ImageToArrayPreprocessor()
sdl = SimpleDatasetLoader(preprocessors=[sp, iap])
(data, labels) = sdl.load(imagePaths, verbose=500)
data = data.astype("float") / 255.0
(trainX, testX, trainY, testY) = train_test_split(
    data, labels, test_size=0.25, random_state=42)
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)

print("[INFO] Compiling the model...")
sgd = SGD(lr=0.005)
model = ShallowNet.build(width=32,height=32,depth=3,classes=3)
model.compile(optimizer=sgd, loss="categorical_crossentropy",
              metrics=["accuracy"])

print("[INFO] Training the network...")
H = model.fit(trainX, trainY, validation_data=(testX, testY),
              epochs=args["epochs"], batch_size=32, verbose=1)

print("[INFO] Evaluating the model...")
predictions = model.predict(testX, batch_size=32)
print(classification_report(testY.argmax(axis=1), predictions.argmax(
    axis=1), target_names=["cat", "dog", "panda"]))

model.save(args["output"])
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, args["epochs"]), H.history["val_acc"], label="val_acc")
plt.title("Training loss and accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.show()
