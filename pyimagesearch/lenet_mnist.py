from lenet import LetNet
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from keras.optimizers import SGD
from sklearn import datasets
from keras import backend as K 
import numpy as np 
import matplotlib.pyplot as plt 
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,  help="path to the downloaded dataset")
ap.add_argument("-e", "--epochs", type=int, default=40, help="epochs to be trained")
ap.add_argument("-m", "--model", required=True, help="hdf5 model to be saved")
args = vars(ap.parse_args())

print("[INFO] loading the mnist dataset")
dataset = datasets.fetch_mldata("MNIST Original", data_home=args["dataset"])
data = dataset.data/255.0
# 数据的规格要和model接口的规格一致，因此这里和model里面都要有格式调整部分
# 第二个调整的必要性是原来的data格式是3维的，model要求4维
if K.image_data_format() == "channels_first":
    data = data.reshape(data.shape[0],1,28,28)
else:
    data = data.reshape(data.shape[0],28,28,1)
(trainX, testX, trainY, testY) = train_test_split(
    data, dataset.target.astype(int), test_size=0.25, random_state=42)
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)


print("[INFO] Compiling the model...")
sgd = SGD(lr = 0.01)
model = LetNet.build(height=28, width=28, depth=1, classes=10)
model.compile(optimizer=sgd, loss="categorical_crossentropy", metrics=["accuracy"])

print("[INFO] Training the model...")
H = model.fit(trainX, trainY, epochs=args["epochs"], validation_data=(testX,testY), batch_size=128, verbose=1)
model.save(args["model"])

print("[INFO] Evaluating the model...")
predictions = model.predict(testX)
print(classification_report(testY.argmax(axis=1), predictions.argmax(axis=1), target_names=[str(x) for x in lb.classes_]))

plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, args["epochs"]), H.history["val_acc"], label="val_acc")
plt.title("Training loss and accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.show()

