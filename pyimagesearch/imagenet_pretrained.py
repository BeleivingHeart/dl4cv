from keras.applications import VGG16
from keras.applications import VGG19
from keras.applications import ResNet50
from keras.applications import InceptionV3
from keras.applications import Xception
from keras.applications import imagenet_utils
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from imutils import paths
import numpy as np
import argparse
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str, default="vgg16",
                help="the pretrained model to be loaded")
args = vars(ap.parse_args())

# load the model
MODELS = {
    "vgg16": VGG16, 
    "vgg19": VGG19,
    "inception": InceptionV3,
    "resnet": ResNet50,
    "xception": Xception
}
if args["model"] not in MODELS.keys():
    raise AssertionError(
        "The model input should match one in the MODELS dictionary.")
Network = MODELS[args["model"]]
model = Network(weights="imagenet")

# prepare the images
print("[INFO] loading images...")
images = []
inputShape = (224, 224)
preprocess = imagenet_utils.preprocess_input
if args["model"] in ("inception", "xception"):
    inputShape = (299, 299)
    preprocess = preprocess_input
from imageNet.myDataset import load_dataset_paths
logPath = 'E:\OneDrive\local-git'
imagePaths, classNames = load_dataset_paths(10,logPath,1)
for path in imagePaths:
    image = load_img(path,target_size=inputShape)  
    image = img_to_array(image)
    images.append(image)
images = np.array(images)
images = preprocess(images)

# predict
print(f"[INFO] classifying images with {args['model']}...")
predictions = model.predict(images)
predictions = imagenet_utils.decode_predictions(predictions)

# tag the images and show
for (i, p) in enumerate(predictions):
    label = p[0][1]
    img = cv2.imread(imagePaths[i])
    cv2.putText(img, f"Label: {label}", (10, 30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255, 0), 2)
    cv2.imshow(f"{args['model']}'s predicton: ", img)
    key = cv2.waitKey(0)
    if key&0xff == ord("q"):
        break
    cv2.destroyAllWindows()
