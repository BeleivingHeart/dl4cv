from nn.lenet import LetNet
from keras.utils import plot_model

model = LetNet.build(28, 28, 3, 10)
plot_model(model, to_file="letnet.png", show_shapes=True)
