import numpy as np


class NeuralNetwork:
    def __init__(self, layers, alpha=0.1):
        self.layers = layers
        self.alpha = alpha
        self.ws = []

        for i in range(len(self.layers)-2):
            w = np.random.randn(self.layers[i]+1, self.layers[i+1]+1)
            self.ws.append(w)
        for i in range(len(self.layers)-2, len(self.layers)-1):
            w = np.random.randn(self.layers[i]+1, self.layers[i+1])
            self.ws.append(w)

    def sigmoid(self, x):
        return 1.0/(1 + np.exp(-x))

    def sigmoid_deriv(self, x):
        return x*(1-x)

    def predict(self, Xs):
        Xs = np.atleast_2d(Xs)
        Xs = np.c_[Xs, np.ones(Xs.shape[0])] # concatenate along second axis
        outs = []
        for j in np.arange(Xs.shape[0]):
            out = Xs[j]
            for i in range(len(self.layers)-1):
                out = self.sigmoid(out.dot(self.ws[i]))
            outs.append(out)
        return np.array(outs).reshape(Xs.shape[0], -1)

    def fit(self, Xs, Ys, epochs=1000):
        verbose = epochs/10
        losses = []
        Xs = np.c_[Xs, np.ones(Xs.shape[0])]
        for epoch in range(epochs):
            for (x, y) in zip(Xs, Ys):
                self.fit_partial(x, y)
            if epoch == 0 or (epoch+1) % verbose == 0:
                loss = self.cal_loss(Xs,Ys)
                print(f"[INFO] loss at epoch#{epoch+1}: {loss}")
                losses.append(loss)
        return losses

    def fit_partial(self, x, y):
        A = [np.atleast_2d(x)]
        for layer in range(len(self.ws)):
            a = self.sigmoid(A[-1].dot(self.ws[layer]))
            A.append(a)
        error = np.atleast_2d(y) - A[-1]

        E = [error]
        for layer in np.arange(len(A)-2, 0, -1):
            e = E[-1].dot(self.ws[layer].T)
            E.append(e)

        E = E[::-1]
        for layer in np.arange(0, len(self.ws)):
            dw = E[layer] * self.alpha * A[layer].T.dot(self.sigmoid_deriv(A[layer+1]))     
            self.ws[layer] += dw

    def cal_loss(self, Xs, Ys):
        p = self.predict(Xs[:,0:-1])
        err = p - Ys
        return (err**2).sum()

    def __str__(self):
        return f"NeuralNetwork: {'-'.join(str(n) for n in self.layers)}"