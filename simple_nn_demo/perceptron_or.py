from perceptron import Perceptron 
import numpy as np

Xs = np.array([[0,0],[0,1],[1,0],[1,1]])
Ys = np.array([0,1,1,1])

P = Perceptron(2)
P.fit(Xs,Ys)

for (x,y) in zip(Xs,Ys):
    p = P.predict(x)
    print(f"[INFO] Input = {x}, Output = {p}, Ground_truth = {y}")
