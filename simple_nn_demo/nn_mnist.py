from neuralnetwork import NeuralNetwork
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn import datasets
import matplotlib.pyplot as plt
import numpy as np

epochs = 100

print("[INFO] loading mnist(sample) dataset...")
digits = datasets.load_digits()
data = digits.data.astype("float")
data = (data - data.min())/(data.max()-data.min())
print(f"[INFO] samples:{data.shape[0]}, dim:{data.shape[1]}")

(trainX, testX, trainY, testY) = train_test_split(data, digits.target, test_size = 0.25)

trainY = LabelBinarizer().fit_transform(trainY)
testY = LabelBinarizer().fit_transform(testY)

print("[INFO] training data...")
nn = NeuralNetwork([data.shape[1],32,16,10])
print(f"[INFO] {nn}")
losses = nn.fit(trainX,trainY,epochs)

print("[INFO] evaluating...")
predictions = nn.predict(testX)
predictions = predictions.argmax(axis = 1)
print(classification_report(testY.argmax(axis=1),predictions))

plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, epochs+1, 10), losses)
plt.title("Training Loss")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.show()


