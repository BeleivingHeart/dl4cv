from neuralnetwork import NeuralNetwork
import numpy as np

nn = NeuralNetwork([2,4,8,1],0.5)
Xs = np.array([[0,0],[0,1],[1,0],[1,1]])
Ys = np.array([[0],[1],[1],[0]])

nn.fit(Xs,Ys,1000)
print(nn.predict(Xs))