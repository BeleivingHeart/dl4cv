from keras.applications import VGG16
from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-i", "--include_top", type=int, default=1, help="path to input archives")
args = vars(ap.parse_args())

print('[INFO] loading vgg16...')
model = VGG16(include_top=args['include_top']>0)
for i, layer in enumerate(model.layers):
    print(f'layer {i} is of type: {layer.__class__.__name__}')