import requests
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-n", "--number",  type=int, default=500,
                help="the number of images we want to download")
ap.add_argument("-o", "--output", required=True,
                help="output path end with backward slash")
args = vars(ap.parse_args())
 
def downimage(i, path):
    # 构建session
    sess = requests.Session()
    # 建立请求头
    headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36",
             "Connection": "keep-alive"}
    # 这个url是联合航空公司验证码，根据访问时间戳返回图片
    url="http://www.tsdm.me/plugin.php?id=oracle:verify"
    # 获取响应图片内容
    image=sess.get(url,headers=headers).content
    # 保存到本地
    with open(path + f"\image{str(i)}.jpg","wb") as f:
        f.write(image)
 
if __name__=="__main__":
    # 获取10张图片
    for i in range(args["number"]):
        downimage(i, args["output"])