from time import time
import numpy as np
from sklearn.datasets import fetch_lfw_people
from sklearn.decomposition import PCA
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.svm import SVC
import matplotlib.pyplot as plt

# load in data
lfw_people = fetch_lfw_people(min_faces_per_person=70)
data = lfw_people.data
n_samples, h, w = lfw_people.images.shape
labels = lfw_people.target
names = lfw_people.target_names
n_classes = names.shape[0]
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.25, random_state=42)

# build estimators
pca = PCA(n_components=150, whiten=True) # whiten must be set to True here
svc = SVC(class_weight='balanced')
params = dict(C=[1e-1, 1e1, 1e2, 1e3, 1e4, 1e5])  # , gamma=[0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1]
model = GridSearchCV(svc, param_grid=params, cv=3)

# reduce dims
t0 = time()
print('[INFO] reducing dimensions...')
pca_trainX = pca.fit_transform(trainX)
pca_testX = pca.transform(testX)
print(f"[TIME] time cost: {time()-t0:.2f}")
print("[INFO] information retention rate: {:.2f}%".format(np.sum(pca.explained_variance_ratio_) * 100))

# fit the model
t0 = time()
model.fit(pca_trainX, trainY)
predictions = model.predict(pca_testX)
print(f'[INFO] best params are: {model.best_params_}')
print(classification_report(testY, predictions, target_names=names))
print(f"[TIME] time cost: {time()-t0}")


# define helper functions
def plot_faces(images, titles, h, w, n_rows=3, n_cols=4):
    fig = plt.figure()
    plt.subplots_adjust(bottom=0, left=.01, right=.99, top=.90, hspace=.35)
    for i in range(n_rows * n_cols):
        plot = fig.add_subplot(n_rows, n_cols, i + 1)
        plot.imshow(images[i].reshape(h, w))
        plot.set_title(titles[i])
        plot.set_axis_off()


def get_titles(preds, labels, names):
    titles = []
    for label, pred in zip(labels, preds):
        true_name = names[label]
        guess_name = names[pred]
        titles.append(f'True: {true_name}\nPred: {guess_name}')
    return titles


# plot results
eigenFaces = pca.components_[:12]
eigenTitles = [f'eigen_face_{i}' for i in range(12)]
plot_faces(eigenFaces, eigenTitles, h=h, w=w)
plt.show()
titles = get_titles(predictions[:12], testY[:12], names)
plot_faces(testX, titles, h=h, w=w)
plt.show()
