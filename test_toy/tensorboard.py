import tensorflow as tf
import os

from tensorflow.python.keras.callbacks import TensorBoard

a = tf.constant(6.5)
squared = tf.square(a,name='squared')

with tf.Session() as sess:
    sess.run(squared)
    with tf.summary.FileWriter('./tensorbordlog',sess.graph) as writer:
        pass
