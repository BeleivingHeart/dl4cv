from skimage.exposure import rescale_intensity
import numpy as np 
import cv2
import argparse

def convolve(image, K):
    (ih, iw) = image.shape[:2]
    (kh, kw) = K.shape[:2]
    pad = (kw-1)//2
    temp = cv2.copyMakeBorder(image,pad,pad,pad,pad,cv2.BORDER_REPLICATE)

    output = np.zeros((ih,iw),dtype=float) 

    for y in np.arange(pad, pad+ih):
        for x in np.arange(pad, pad+iw):
            roi = temp[y-pad:y+pad+1,x-pad:x+pad+1]
            s = (roi*K).sum()
            output[y-pad,x-pad] = s
    
    output = rescale_intensity(output, in_range=(0,255))
    output = (output * 255).astype("uint8")
    return output

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
                help="path to the input image")
args = vars(ap.parse_args())

small_blur = np.ones((7,7),dtype=float)*(1.0/(7.0*7.0))
large_blur = np.ones((21,21),dtype=float)*(1.0/(21.0*21.0))
sharpen = np.array(([0,-1,0],
                [-1,5,-1],
                [0,-1,0]),dtype="int")
laplacian = np.array(([0,1,0],
                [1,-4,1],
                [0,1,0]),dtype=int)
shapen = np.array(([0,-1,0],
                [-1,5,-1],
                [0,-1,0]),dtype=int)
sobelX = np.array(([-1,0,1],
                [-2,0,2],
                [1,0,1]),dtype=int)
sobelY = np.array(([-1,-2,-1],
                [0,0,0],
                [1,2,1]),dtype=int)
emboss = np.array(([-2,-1,0],
                [-1,1,1],
                [0,1,2]),dtype=int)

kernel_bank = (
    ("sharpen",sharpen),
    ("laplacian",laplacian),
    ("sobelX",sobelX),
    ("sobelY",sobelY),
    ("emboss",emboss)
)

image = cv2.imread(args["input"])
gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
output = gray.copy()
output2 = gray.copy()

for (kernel_name,kernel) in kernel_bank:
    output = convolve(gray, kernel)
    output2 = cv2.filter2D(gray,-1,kernel)
    print(f"[INFO] applying {kernel_name}")

    cv2.imshow("Color",image)
    cv2.imshow("My convolution: "+kernel_name,output)
    cv2.imshow("cv convolution: "+kernel_name,output2)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    




