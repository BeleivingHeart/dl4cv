import cv2
import keras # load_img
import imutils # load_image
import numpy as np 

image1 = keras.preprocessing.image.load_img('/Users/mac/Downloads/201052118560.jpg')
image1 = np.array(image1)[:,:,::-1]
print(image1.shape)
image3 = cv2.imread('/Users/mac/Downloads/201052118560.jpg',cv2.IMREAD_COLOR)
image3 = cv2.cvtColor(image3,cv2.COLOR_BGR2GRAY)
print(image3.shape)

for img in [image1,image3]:
    cv2.imshow('image',img)
    cv2.waitKey(0)


"""
SUMMERY
load_img load in rgb
imread load in bgr

"""