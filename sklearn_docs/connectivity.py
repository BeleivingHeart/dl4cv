
from scipy.misc import face, imresize

# load in face data
from sklearn.cluster import AgglomerativeClustering
from sklearn.feature_extraction import grid_to_graph
import matplotlib.pyplot as plt
import numpy as np

face = face(gray=True)
face = imresize(face, 0.1)/255.0
X = face.reshape(-1,1)

connectivity = grid_to_graph(*face.shape)
n_clusters = 15
ward = AgglomerativeClustering(n_clusters=n_clusters,connectivity=connectivity)
ward.fit(X)
labels = ward.labels_.reshape(face.shape)
print(type(connectivity))
plt.figure()
plt.imshow(face, cmap=plt.cm.gray)
for i in range(n_clusters):
    plt.contour(labels==i,colors=[plt.cm.rainbow(i/float(n_clusters))])
plt.xticks()
plt.yticks()
plt.show()
