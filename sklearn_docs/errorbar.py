"""
http://scikit-learn.org/stable/auto_examples/svm/plot_svm_anova.html#sphx-glr-auto-examples-svm-plot-svm-anova-py
"""
from time import time

from sklearn.datasets import load_digits
import numpy as np
import matplotlib.pyplot as plt

# load in data
from sklearn.feature_selection import SelectPercentile, f_classif
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC

digits = load_digits()
X, y = digits.data, digits.target
n_samples = len(X)
# and add in some noise
noise = np.random.rand(n_samples, 100)
X = np.hstack((X, noise))

# construct the pipeline
estimators = [('anova_fiter',SelectPercentile(f_classif)),('clf',SVC())]
pipe = Pipeline(steps=estimators)
percentiles = np.arange(10,101,10)

# loop over the percentiles
score_means = []
score_stds = []
for p in percentiles:
    print(f'[INFO] scoring {p} percent')
    t0 = time()
    pipe.set_params(anova_fiter__percentile=p)
    score = cross_val_score(pipe, X, y)
    score_means.append(score.mean())
    score_stds.append(score.std())
    print(f'Time cost: {time()-t0:.2f}')

plt.errorbar(percentiles, score_means, score_stds)
plt.xlabel('percentile of features to keep')
plt.ylabel('score')
plt.title(r'SVM performance for different percentile of features to keep')
plt.show()