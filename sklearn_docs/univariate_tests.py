"""
achived 0.90 with 10 features
achived 0.98 with 40 features, this is as good as no reduction
"""
import os
from time import time

from sklearn.datasets import load_digits

# load in data
from sklearn.feature_selection import chi2, f_classif, mutual_info_classif, SelectKBest, f_regression
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.svm import SVC

digits = load_digits()
X, y = digits.data, digits.target
trainX, testX, trainY, testY = train_test_split(X, y, test_size=0.25, random_state=42)

# set up a list of univariate tests
tests = [chi2, f_classif, mutual_info_classif]
test_names = ['chi2', 'f_classif', 'mutual_info_classif']

# classification without feature reduction
print('[INFO] fitting without feature reduction...')
t0 = time()
clf = SVC(kernel='linear', C=0.3)
clf.fit(trainX, trainY)
print(f'Time cost: {time()-t0:.2f}')
preds = clf.predict(testX)
base_path = '/Users/mac/local-git/dl4cv/sklearn_docs/output'
path = os.path.sep.join([base_path, 'svc_no_reduction.txt'])
with open(path, 'w') as wf:
    wf.write(classification_report(testY, preds))

#loop over the univariate test
for test, name in zip(tests, test_names):
    anova_filter = SelectKBest(test, k=40)
    clf = SVC(kernel='linear', C=0.3)
    pipe = make_pipeline(anova_filter, clf)
    t0 = time()
    print(f'[INFO] fitting with univariate test: [{name}]...')
    pipe.fit(trainX, trainY)
    print(f'Time cost: {time()-t0:.2f}')
    preds = pipe.predict(testX)
    path = path = os.path.sep.join([base_path, name + '.txt'])
    with open(path, 'w') as wf:
        wf.write(classification_report(testY, preds))
