import numpy as np
import matplotlib.pyplot as plt
from sklearn.feature_selection import f_regression, mutual_info_regression

X = np.random.rand(1000, 3)
y = X[:, 0] + np.sin(6 * np.pi * X[:, 1]) + np.random.rand(1000) * 0.1
f_scores, pval = f_regression(X, y)
f_scores /= f_scores.max()
mutual_scores = mutual_info_regression(X, y)

print(len(f_scores), len(mutual_scores))
plt.figure(figsize=(15, 5))
for i in range(3):
    plt.subplot(1,3,i+1)
    plt.scatter(X[:,i], y, c='orange', s=20, edgecolors='black')
    plt.title(f'f_regression score: {f_scores[i]:.2f}\n mutual score: {mutual_scores[i]:.2f}')
plt.show()