from sklearn.datasets import load_iris
import numpy as np
import matplotlib.pyplot as plt
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.svm import SVC

iris = load_iris()
data, labels = iris.data, iris.target

# add in noise to data
noise = np.random.uniform(low=0, high=0.1, size=(len(data),20))
data = np.hstack((data, noise))

plt.figure()
plt.clf()
x_indices = np.arange(data.shape[1])

# plot the first bar -- the bar of classifier before feature selection is applied
clf = SVC(kernel='linear')
clf.fit(data, labels)
svm_weights = (clf._get_coef()**2).sum(axis=0)
print(svm_weights.shape, x_indices.shape)
svm_weights /= svm_weights.max()
plt.bar(x_indices-0.45, svm_weights, width=0.2, label= r'svm_weight_unselected', color='orange')
# plot the second bar -- the bar after selection
selector = SelectKBest(f_classif, 4)
selector.fit(data, labels)
selected_data = selector.transform(data)
selector_weights = selector.pvalues_
selector_weights /= selector_weights.max()
plt.bar(x_indices-0.25, selector_weights, width=0.2, label= r'weight_selected', color='navy')
# plot the third bar -- the bar of svm after selection
svm_selection = SVC(kernel='linear')
svm_selection.fit(selected_data, labels)
svm_weights_selected = (svm_selection._get_coef()**2).sum(axis=0)
svm_weights_selected /= svm_weights_selected.max()
plt.bar(x_indices-0.05, selector_weights, width=0.2, label= r'svm_weight_selected', color='green')


plt.legend()
plt.show()
