from time import time

import matplotlib.pyplot as plt
from sklearn.datasets import load_digits
from sklearn.feature_selection import RFE
from sklearn.svm import SVC

digits = load_digits()
data, target = digits.data, digits.target
clf = SVC(kernel='linear')
feature_reducer = RFE(estimator=clf, step=1, n_features_to_select=1)
t0 = time()
feature_reducer.fit(data, target)
print(f'[INFO] cost {time()-t0:.2f}s to fit')
rankings = feature_reducer.ranking_.reshape(8, 8)

plt.matshow(rankings, cmap=plt.cm.Blues)
plt.colorbar()
plt.title('Dependency rankings on each pixel')
plt.show()