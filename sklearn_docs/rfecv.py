from time import time

from sklearn.datasets import make_classification
import matplotlib.pyplot as plt
from sklearn.feature_selection import RFECV
from sklearn.model_selection import StratifiedKFold
from sklearn.svm import SVC

X, y = make_classification(n_features=25, n_informative=3, n_classes=8, n_clusters_per_class=1, n_redundant=2,
                           n_repeated=0)
clf = SVC(kernel='linear', C=1)
skf = StratifiedKFold(2)
feature_reducer = RFECV(estimator=clf, cv=skf, step=1, scoring='accuracy')
t0 = time()
feature_reducer.fit(X, y)
print(f'[INFO] optimal number of features is {feature_reducer.n_features_}')
print(f'Time cost: {time()-t0:.2f}')

plt.figure()
x_indices = range(1,len(feature_reducer.grid_scores_)+1)
plt.plot(x_indices,feature_reducer.grid_scores_)
plt.xlabel('number of features to keep')
plt.ylabel('score')
plt.show()
