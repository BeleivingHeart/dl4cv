"""
http://scikit-learn.org/stable/auto_examples/plot_compare_reduction.html#sphx-glr-auto-examples-plot-compare-reduction-py
"""
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA, NMF
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
import numpy as np
import matplotlib.pyplot as plt

# construct GridSearch object
estimators = [('feature_reducer', PCA()), ('clf', LinearSVC())]
pipe = Pipeline(steps=estimators)
features = [4, 10, 16]
C_params = np.logspace(-1, 2, 3)
grid_params = [{
    'feature_reducer': [PCA(iterated_power=7), NMF()],
    'feature_reducer__n_components': features,
    'clf__C': C_params
}, {
    'feature_reducer': [SelectKBest(chi2)],
    'feature_reducer__k': features,
    'clf__C': C_params
}]
grid = GridSearchCV(pipe, param_grid=grid_params, cv=3)

# load in data and fit
digits = load_digits()
X, y = digits.data, digits.target
grid.fit(X, y)

# assess the scores
reducer_names = ['PCA', 'NMF', 'SelectKBest(chi2)']
scores = grid.cv_results_['mean_test_score']
# reshape into (n_c_params, n_reducers, n_features)
scores = scores.reshape(len(C_params), len(reducer_names), len(features))
# choose scores of best C param, and now the shape become (n_reducers, n_features)
scores = scores.max(axis=0)
bar_offests = np.arange(0, len(features)) * (len(reducer_names) + 1) + .5
plt.figure()
colors = 'rgbon'
for i, (reducer_scores, reducer) in enumerate(zip(scores, reducer_names)):
    plt.bar(bar_offests+i, reducer_scores, label=reducer, color=colors[i])
plt.xlabel(r'n_features to keep')
plt.ylabel(r'scores for the best C')
plt.xticks(bar_offests+2, features)
plt.legend()
plt.show()
