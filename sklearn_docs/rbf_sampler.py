import numpy as np
from sklearn.kernel_approximation import RBFSampler
from sklearn.linear_model import SGDClassifier
from sklearn.neighbors import NearestNeighbors
from sklearn.svm import LinearSVC

X = np.array([[0,0],[0,1],[1,0],[1,1]])
y = np.array([0,1,1,0])
rbf = RBFSampler()
X_rbf = rbf.fit_transform(X)
clf = LinearSVC()
score = clf.fit(X_rbf,y).score(X_rbf,y)
print(score)