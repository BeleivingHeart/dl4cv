import argparse

import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import make_blobs
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split


def sigmoid_activation(x):
    return 1.0 / (1 + np.exp(-x))


def predict(X, W):
    preds = sigmoid_activation(X.dot(W))
    preds[preds <= 0.5] = 0
    preds[preds >= 0.5] = 1
    return preds


ap = argparse.ArgumentParser()
ap.add_argument("-e", "--epochs", type=float, default=100, help="# of epochs")
ap.add_argument("-a", "--alpha", type=float,
                default=0.01, help="learning rate")
args = vars(ap.parse_args())

(coordinates, lables) = make_blobs(n_samples=1000, n_features=2,
                    centers=2, cluster_std=1.5, random_state=1)
lables = lables.reshape((lables.shape[0], 1))

coordinates = np.c_[coordinates, np.ones(coordinates.shape[0])]

(train_coordinates, test_coordinates, train_labels, test_labels) = train_test_split(
    coordinates, lables, test_size=0.5, random_state=42)

print("[INFO] training...")
W = np.random.randn(coordinates.shape[1], 1)
losses = []

for epoch in np.arange(args["epochs"]):
    preds = sigmoid_activation(train_coordinates.dot(W))
    error = preds - train_labels
    loss = np.sum(error ** 2)
    losses.append(loss)

    gradient = train_coordinates.T.dot(error)
    W += -args["alpha"] * gradient
    if epoch == 0 or (epoch+1) % 5 == 0:
        print("[INFO] epoch = {}, loss ={:.7f}".format(int(epoch + 1), loss))

print("[INFO] evaluating...")
preds = predict(test_coordinates, W)
print(classification_report(test_labels, preds))


plt.style.use("ggplot")
plt.figure()
plt.title("Data")
plt.scatter(test_coordinates[:, 0], test_coordinates[:, 1], s=30, c=test_labels.flatten())

plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), losses)
plt.title("Training Loss")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.show()