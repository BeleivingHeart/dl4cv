import argparse
from keras.models import load_model
from imutils import paths
import cv2
from keras.preprocessing.image import img_to_array
from imutils import contours
from pyimagesearch.utils import preprocess
import numpy as np

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
                help="path to the input images")
ap.add_argument("-m", "--model", required=True,
                help="path to the input model")
args = vars(ap.parse_args())

# load the model
model = load_model(args["model"])

# pool some images from the dataset
imagePaths = list(paths.list_images(args["input"]))
imagePaths = np.random.choice(imagePaths, size=(10,), replace=False)
for path in imagePaths:
    image = cv2.imread(path, cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # in case some digits are very close to the image edge
    gray = cv2.copyMakeBorder(gray, 20, 20, 20, 20, cv2.BORDER_REPLICATE)
    thresh = cv2.threshold(
        gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    cnts = cv2.findContours(thresh, mode=cv2.RETR_EXTERNAL,
                            method=cv2.CHAIN_APPROX_SIMPLE)
    cnts = sorted(cnts, cv2.contourArea, reverse=True)[:4]
    boxes = contours.sort_contours(cnts, method="left-to-right")[1]
    predictions = []
    for (x, y, w, h) in boxes:
        roi = gray[y-5:y+h+5, x-5:x+w+5]
        roi = preprocess(roi, 28, 28)
        roi = img_to_array(roi)/255.0
        roi = np.expand_dims(roi, axis=0)
        pred = model.predict(roi).argmax(axis=1)[0]
        predictions.append(pred)
        cv2.putText(image, str(pred), x-5, y-5,
                    cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)
        cv2.rectangle(image, (x-2, y-2), (x+w+2, y+h+2),
                      (255, 0, 0), thickness=1)
    print(f"[INFO] captcha: {''.join(predictions)}")
    cv2.imshow("Captcha", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
