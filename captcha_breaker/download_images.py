import argparse
import requests
import time
import os

ap = argparse.ArgumentParser()
ap.add_argument("-n", "--number",  type=int, default=500,
                help="the number of images we want to download")
ap.add_argument("-o", "--output", required=True,
                help="output path end with backward slash")
args = vars(ap.parse_args())

url = "http://www.tsdm.me/plugin.php?id=oracle:verify"
headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36",
             "Connection": "keep-alive"}
total = 0

for i in range(0, args["number"]):
    try:
        r = requests.session().get(url, headers=headers, timeout=60)
        p = os.path.sep.join([args["output"], f"{str(total).zfill(5)}.png"])
        jpg = open(p, "wb")
        jpg.write(r.content)
        jpg.close()
        total += 1
        print(f"[INFO] downloading {p}")

    except:
        print("[ERROR] error downloading image")
    time.sleep(0.01)
