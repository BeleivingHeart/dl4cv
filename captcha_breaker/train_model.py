import argparse
from imutils import paths
from pyimagesearch.datasets import SimpleDatasetLoader
from keras.preprocessing.image import img_to_array
import numpy as np 
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from pyimagesearch import LetNet 
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint
from pyimagesearch.utils import CaptchaHelper
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt 

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset")
ap.add_argument("-e", "--epochs", type=int, default=40,
                help="epochs to be trained")
ap.add_argument("-o", "--output", required=True, help="output .h5 file")
args = vars(ap.parse_args())

# load the images
print("[INFO] loading the images...")
imagePaths = list(paths.list_images(args["dataset"]))
p1 = CaptchaHelper(28, 28)
sdl = SimpleDatasetLoader(preprocessors=[p1])
(data, labels) = sdl.load(imagePaths) # not sure if I am loading color images

# preprocess the data and labels
data = data/255.0
(trainX, testX, trainY, testY) = train_test_split(
    data, labels, test_size=0.25, random_state=42)
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)

# build the model
model = LetNet.build(28, 28, 1, 10)
callbacks = [ModelCheckpoint(args["output"], mode="min", verbose=1)]
sgd = SGD(lr=0.01, momentum=0.9,nesterov=True,decay=0.01/args["epochs"])
model.compile(optimizer=sgd, loss="categorical_crossentropy", metrics=['accuracy'])

# train the model
print("[INFO] training the model...")
H = model.fit(trainX,trainY,batch_size=128,epochs=args["epochs"],verbose=1,callbacks=callbacks)

# evaluate the model
print("[INFO] evaluating the model...")
predictions = model.predict(testX, batch_size=128)
print(classification_report(testY.argmax(axis=1),predictions.argmax(axis=1)))

# plot the history
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, args["epochs"]), H.history["val_acc"], label="val_acc")
plt.title("Training loss and accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig(args["output"]+"\\captcha.png")
plt.show()
