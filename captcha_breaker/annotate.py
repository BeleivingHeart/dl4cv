from imutils import paths
import imutils
import argparse
import os
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
                help="path to the input captcha images")
ap.add_argument("-o", "--output", required=True,
                help="output path of the annotated images")
args = vars(ap.parse_args())

counts = {}
imagePaths = list(paths.list_images(args['input']))

for (i, path) in enumerate(imagePaths):
    try:
        image = cv2.imread(path)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.copyMakeBorder(gray, 8, 8, 8, 8, cv2.BORDER_REPLICATE)
        thresh = cv2.threshold(
            gray, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)[1]
        cnts = cv2.findContours(
            thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if imutils.is_cv2() else cnts[1]
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:4]

        for contour in cnts:
            (x, y, w, h) = cv2.boundingRect(contour)
            roi = gray[y-5:y+h+5, x-5:x+w+5]
            cv2.imshow("ROI", imutils.resize(roi, width=28))
            key = cv2.waitKey(0)
            if ord(key) == '`':
                print("[INFO] ignoring character")
                continue
            else:
                count = counts.get(key, 1)
                save_path = os.path.sep.join([args["output"], key])
                if not os.path.exists(save_path):
                    os.makedirs(save_path)
                save_path = os.path.sep.join(
                    [save_path, f"{str(count).zfill(3)}.jpg"])
                cv2.imwrite(save_path, roi)
                counts[key] = count+1

    except KeyboardInterrupt:
        print("[INFO] manually leaving script...")
        break

    except:
        print("[INFO] skipping image...")
