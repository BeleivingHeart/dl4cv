import tensorflow as tf

unfinished

def train_step(net_out, meta, ground_truth):
    """Calculate loss between yolo-net batch outputs and ground-truths
    :param meta: a dict of scalar weights for calculating loss
    :param net_out: batch output from yolo-v1 network
    :param ground_truth: a dict of class_probs, object_exists, actual_areas, up_lefts, bottom_right and coordinates
    :return: a scalar tensor loss
    """

    # scalar weights
    lambda_coord = meta['lambda_coor']
    lambda_noobj = meta['lambda_noobj']
    has_obj_conf_scale = meta['has_obj_conf_scale']
    class_probs_scale = meta['class_probs_scale']
    S = meta['S']
    SS = S**2
    B = meta['B']
    C = meta['C']

    # ground truths
    sizeB = [None, SS, B]
    sizeC = [None, SS, C]
    with tf.name_scope('ground_truths') as gt:
        _classProbs = tf.placeholder(dtype=tf.float32,shape=sizeC)
        _objectExists = tf.placeholder(dtype=tf.float32,shape=sizeB)
        _actualAreas = tf.placeholder(dtype=tf.float32,shape=sizeB)
        _coordinates = tf.placeholder(dtype=tf.float32,shape=sizeB+[4])
        _upLeft = tf.placeholder(dtype=tf.float32,shape=sizeB+[2])
        _bottomRight = tf.placeholder(dtype=tf.float32,shape=sizeB+[2])

    # from net_out use only coordinates to determine loss weights and true confidence score for bounding boxes
    predicted_coordinates = net_out[:,SS*(C+B):]
    predicted_coordinates = tf.reshape(tensor=predicted_coordinates,shape=[-1,SS,B,4])
    predicted_centers = predicted_coordinates[:,:,:,:2]
    predicted_wh = tf.pow(x=predicted_coordinates[:,:,:,2:],y=2) * S # unify height and width to cell scale
    predicted_areas = predicted_wh[:,:,:,0] * predicted_wh[:,:,:,1]
    predicted_upLeft = predicted_centers - predicted_wh/2
    predicted_bottomRight = predicted_centers - predicted_wh/2
    # calculate IoU
    intersect_upLeft = tf.maximum(x=predicted_upLeft,y=_upLeft)
    intersect_bottomRight = tf.minimum(x=predicted_bottomRight,y=_bottomRight)
    intersect_wh = tf.maximum(x=intersect_bottomRight-intersect_upLeft,y=0)
    intersect_areas = intersect_wh[:,:,:,0] * intersect_wh[:,:,:,1] # [batch, SS, B]
    IoU = tf.realdiv(x=intersect_areas, y=predicted_areas+_actualAreas-intersect_areas)
    # calculate lambda_Lijobj for coordinates loss
    is_bestBox = tf.equal(x=IoU,y=tf.reduce_max(input_tensor=IoU,axis=[2],keepdims=True))
    is_bestBox = tf.cast(x=is_bestBox,dtype=tf.float32)
    objectExists_and_best = is_bestBox * _objectExists
    Lij_obj = tf.concat(values=4*[tf.expand_dims(input=objectExists_and_best,axis=-1)],axis=-1)
    coordinates_weights = lambda_coord * Lij_obj
    # calculate bestBox vs noObject confidence loss weight
    confidence_weights = lambda_noobj*(1.0-objectExists_and_best) + has_obj_conf_scale*objectExists_and_best
    # calculate classification weights
    classification_weights = class_probs_scale * tf.tile(input=_objectExists[:,:,:1],multiples=[1,1,C])

    # align loss weights with reformed ground truths
    classification_weights = tf.layers.flatten(inputs=classification_weights)
    confidence_weights = tf.layers.flatten(inputs=confidence_weights)
    coordinates_weights = tf.layers.flatten(inputs=coordinates_weights)
    penalize_weights = tf.concat(values=[classification_weights,confidence_weights,coordinates_weights],axis=-1)

    true_classProbs = tf.layers.flatten(inputs=_classProbs)
    true_confidence = tf.layers.flatten(inputs=objectExists_and_best)
    true_coordinates = tf.layers.flatten(inputs=_coordinates)
    true_labels = tf.concat(values=[true_classProbs,true_confidence,true_coordinates],axis=-1)

    # calculate loss
    loss = tf.square(x=net_out-true_labels)
    loss = loss * penalize_weights

