import tensorflow as tf
import numpy as np
from math import ceil

class my_model:

    def __init__(self, input_shape=[None, 32, 32, 3], n_classes=10):
        self.x_ = tf.placeholder(dtype=tf.float32, shape=input_shape, name='x_')
        # y_ is 1d tensor
        self.y_ = tf.placeholder(dtype=tf.int32, shape=[None, 1], name='y_')
        self.is_training = tf.placeholder(dtype=tf.bool)
        with tf.name_scope('feed_forward') as feed_forward:
            # conv layers
            net = tf.layers.conv2d(inputs=self.x_, filters=64, kernel_size=[3, 3], strides=1, activation=tf.nn.relu,
                                   name='conv_1')
            net = tf.layers.batch_normalization(inputs=net, training=self.is_training, name='conv_1_bn')
            net = tf.layers.max_pooling2d(inputs=net, pool_size=[2, 2], strides=2, padding='valid', name='conv_1_pool')

            net = tf.layers.conv2d(inputs=net, filters=64, kernel_size=[3, 3], strides=1, activation=tf.nn.relu,
                                   name='conv_2')
            net = tf.layers.batch_normalization(inputs=net, training=self.is_training, name='conv_2_bn')
            net = tf.layers.max_pooling2d(inputs=net, pool_size=[2, 2], strides=2, padding='valid', name='conv_2_pool')

            net = tf.layers.conv2d(inputs=net, filters=32, kernel_size=[3, 3], strides=1, activation=tf.nn.relu,
                                   name='conv_3')
            net = tf.layers.batch_normalization(inputs=net, training=self.is_training, name='conv_3_bn')
            net = tf.layers.max_pooling2d(inputs=net, pool_size=[2, 2], strides=2, padding='valid', name='conv_3_pool')

            # dense layers
            net = tf.layers.flatten(inputs=net, name='flatten_layer')
            net = tf.layers.dense(inputs=net, units=64, activation=tf.nn.relu, name='dense_64')
            net = tf.layers.batch_normalization(inputs=net, training=self.is_training, name='dense_bn')
            net = tf.layers.dropout(inputs=net, rate=0.5, training=self.is_training, name='dense_dropout')
            self.logits = tf.layers.dense(inputs=net, units=n_classes, activation=tf.nn.softmax, name='logits')

        with tf.name_scope('loss') as loss_scope:
            self.train_loss = tf.reduce_mean(tf.losses.sparse_softmax_cross_entropy(labels=self.y_, logits=self.logits))
            self.val_loss = tf.reduce_mean(tf.losses.sparse_softmax_cross_entropy(labels=self.y_, logits=self.logits))

            self.train_loss_summary = tf.summary.scalar('loss_train', self.train_loss)
            self.val_loss_summary = tf.summary.scalar('loss_val', self.val_loss)

        with tf.name_scope('optimization') as opt_scope:
            initial_learning_rate = 1e-2
            global_step = tf.Variable(initial_value=0, trainable=False, name='global_step')
            learning_rate = tf.train.exponential_decay(learning_rate=initial_learning_rate,
                                                       global_step=global_step, decay_steps=500, decay_rate=0.9,
                                                       staircase=True, name='learning_rate')

            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                self.train_op = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(self.train_loss,
                                                                                                        global_step=global_step)

            tf.summary.scalar('global_step_summary', global_step)
            tf.summary.scalar('learning_rate_summary', learning_rate)

        self.summary_merged = tf.summary.merge_all()
        init = tf.global_variables_initializer()

        # set up session
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

        # set up summary writer
        self.writer = tf.summary.FileWriter(logdir='./log')
        self.sess.run(init)

        # set up model saver
        # self.saver = tf.train.Saver(max_to_keep=None,)

    def train(self, epochs):
        # prepare dataset
        (trainX, trainY), (testX, testY) = tf.keras.datasets.cifar10.load_data()
        trainY,testY = trainY.astype(np.int32).flatten(),testY.astype(np.int32).flatten()
        epoch_size = len(trainY)
        batch_size = 256
        steps = ceil(epochs * epoch_size / float(batch_size))
        verbose = int(epoch_size / batch_size)
        val_size = len(testY)

        # set up datasets
        train_set = tf.data.Dataset.from_tensor_slices((trainX, trainY))
        train_set = train_set.shuffle(buffer_size=10000).batch(batch_size).repeat().make_one_shot_iterator().get_next()

        test_set = tf.data.Dataset.from_tensor_slices((testX, testY))
        test_set = test_set.batch(batch_size).shuffle(buffer_size=10000).repeat().make_one_shot_iterator().get_next()

        for step in range(steps):
            (batch_trainX, batch_trainY) = self.sess.run(fetches=train_set)

            # log loss and record summary for every epoch
            if step % verbose == 0:
                (batch_testX, batch_testY) = self.sess.run(fetches=test_set)

                # trainset evaluation
                train_logits, train_loss, summary_merged = self.sess.run(
                    fetches=[self.logits, self.train_loss, self.summary_merged],
                    feed_dict={self.x_: batch_trainX, self.y_: batch_trainY, self.is_training: False})
                train_class_ids = np.argmax(train_logits, axis=1)
                train_correct_prediction = np.equal(train_class_ids, batch_trainY).astype(np.float32)
                train_accuracy = np.mean(train_correct_prediction)

                # testset evaluation
                val_logits, val_loss, val_loss_summary = self.sess.run(
                    fetches=[self.logits, self.val_loss, self.val_loss_summary],
                    feed_dict={self.x_: batch_testX, self.y_: batch_testY, self.is_training: False})
                val_class_ids = np.argmax(val_logits, axis=1)
                val_correct_prediction = np.equal(val_class_ids, batch_testY).astype(np.float32)
                val_accuracy = np.mean(val_correct_prediction)

                print('[INFO] step: %05d/%05d, train_loss: %.2f, train_acc: %.2f, val_loss: %.2f, val_acc: %.2f'
                      % (step, steps, train_loss, train_accuracy, val_loss, val_accuracy))

                self.writer.add_summary(summary_merged, step)
                self.writer.add_summary(val_loss_summary, step)

            self.train_op.run(session=self.sess,
                              feed_dict={self.x_: batch_trainX, self.y_: batch_trainY, self.is_training: True})

        # save the model
#         import os
#         base_path = './save'
#         if not os.path.exists(base_path):
#             os.mkdir(base_path)
#         model_path = os.path.sep.join([base_path,'model'])
#         file_path = self.saver.save(sess=self.sess,save_path=model_path)
#         print('[INFO] model saved to %s...'%(file_path))