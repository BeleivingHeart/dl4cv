import sys
sys.path.append(r'E:\OneDrive\local-git\dl4cv\pyimagesearch')
print(f'Appended [pyimagsearch] to sys paths')
IMAGES_PATH = r"F:\BaiduNetdiskDownload\train"
NUM_CLASSES = 2
NUM_VAL_IMAGES = 1250 * NUM_CLASSES
NUM_TEST_IMAGES = 1250 * NUM_CLASSES
TRAIN_HDF5 = r"E:\OneDrive\local-git\HDF5s\dogs_vs_cats\train.hdf5"
VAL_HDF5 = r"E:\OneDrive\local-git\HDF5s\dogs_vs_cats\val.hdf5"
TEST_HDF5 = r"E:\OneDrive\local-git\HDF5s\dogs_vs_cats\test.hdf5"
MODEL_PATH = "output/alexnet_dogs_vs_cats.model"
DATASET_MEAN = "output/dogs_vs_cats_mean.json"
OUTPUT_PATH = "output"