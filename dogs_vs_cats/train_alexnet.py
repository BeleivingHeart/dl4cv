import argparse
import matplotlib
matplotlib.use('Agg')
ap = argparse.ArgumentParser()
ap.add_argument("-b", "--batchSize", type=int, default=1,
                help="batch size")
ap.add_argument("-e", "--epochs", type=int, default=1,
                help="epochs")
args = vars(ap.parse_args())
bs = args['batchSize']
epochs = args['epochs']

from config import dogs_vs_cats_config as config

# set up data generator
from keras.preprocessing.image import ImageDataGenerator
aug = ImageDataGenerator(rotation_range=20, width_shift_range=0.2, height_shift_range=0.2,
                         shear_range=0.15, zoom_range=0.15, horizontal_flip=True, fill_mode='nearest')
from preprocessing.simplepreprocessor import SimplePreprocessor
from preprocessing.patchpreprocessor import PatchPreprocessor
from preprocessing.imagetoarraypreprocessor import ImageToArrayPreprocessor
from preprocessing.meanpreprocessor import MeanPreprocessor
sp = SimplePreprocessor(227,227)
pp = PatchPreprocessor(227,227)
iap = ImageToArrayPreprocessor()
import json
with open(config.DATASET_MEAN,'r') as rf:
    means = json.load(rf)
mp = MeanPreprocessor(rMean=means['R'],gMean=means['G'],bMean=means['B'])
from IO.hdf5datasetgenerator import HDF5DatasetGenerator
trainGen = HDF5DatasetGenerator(dbPath=config.TRAIN_HDF5,batchSize=bs,preprocessors=[pp,mp,iap])
valGen = HDF5DatasetGenerator(dbPath=config.VAL_HDF5,batchSize=bs,preprocessors=[sp,mp,iap])

# set up model
from nn.alexnet import AlexNet
model = AlexNet.build(227,227,3,2)
from keras.optimizers import Adam
optimizer = Adam(lr=1e-3)
model.compile(loss='binary_crossentropy',optimizer=optimizer,metrics=['accuracy'])

# fit model
from callbacks import TrainingMonitor
import os
figPath = os.path.sep.join([config.OUTPUT_PATH, f'{os.getpid()}.png'])
callbacks = [TrainingMonitor(figPath)]
model.fit_generator(
    trainGen.generator(),
    steps_per_epoch=valGen.numImages/bs,
    validation_data=valGen.generator(),
    validation_steps = valGen.numImages/bs,
    epochs=epochs,
    max_queue_size=bs*2,
    callbacks=callbacks,
    verbose=1
)

# save model
print('[INFO] serializing model...')
model.save(config.MODEL_PATH, overwrite=True)

# close datasets
trainGen.close()
valGen.close()

