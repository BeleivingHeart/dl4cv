import config.dogs_vs_cats_config as config
import sys
# load in paths
from imutils import paths
from sklearn.preprocessing import LabelEncoder
imagePaths = list(paths.list_images(config.IMAGES_PATH))
labels = [path.split('.')[0] for path in imagePaths]
le = LabelEncoder()
labels = le.fit_transform(labels)

# spilt them
from sklearn.model_selection import train_test_split
(trainX, testX, trainY, testY) = train_test_split(imagePaths, labels,
                                                  test_size=config.NUM_TEST_IMAGES, stratify=labels, random_state=42)
(trainX, valX, trainY, valY) = train_test_split(trainX, trainY,
                                                test_size=config.NUM_VAL_IMAGES, stratify=trainY, random_state=42)

datasets = [
    ('train',trainX,trainY,config.TRAIN_HDF5),
    ('test',testX, testY, config.TEST_HDF5),
    ('val', valX, valY, config.VAL_HDF5)
]

# build database
import progressbar
from IO.hdf5datasetwriter import HDF5DatasetWriter
import cv2
from preprocessing import AspectAwarePreprocessor
import numpy as np 
aap = AspectAwarePreprocessor(256,256)
(R, G, B) = ([], [], [])
for (dtype, xs, ys, outPath) in datasets:
    # initialize progress bar
    widgets = [f"Writing to {outPath}: ", progressbar.Percentage(
    ), ' ', progressbar.Bar(), " ", progressbar.ETA()]
    pBar = progressbar.ProgressBar(max_value=len(ys),widgets=widgets).start()

    database = HDF5DatasetWriter((len(ys),256,256,3),outPath)
    for (i,(x,y)) in enumerate(zip(xs, ys)):
        image = cv2.imread(x)
        image = aap.preprocess(image)
        database.add([image],[y])
        if(dtype == 'train'):
            b,g,r = cv2.mean(image)[:3]
            R.append(r)
            G.append(g)
            B.append(b)
        pBar.update(i)

    pBar.finish()
    database.close()

print('[INFO] serializing bgr values...')
D = {'R':np.mean(R), 'G':np.mean(G), 'B':np.mean(B)}
import json
with open(config.DATASET_MEAN,'w') as wf:
    json.dump(D,wf, indent=2)