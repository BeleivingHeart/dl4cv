import h5py, pickle
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.pipeline import Pipeline
from time import time
import numpy as np 

ap = ArgumentParser()
ap.add_argument("-j", "--jobs", type=int, default=-1,
                help="how many threads to use for learning")
ap.add_argument("-d", "--database", required=True, help="directory to the .h5 files")
ap.add_argument("-m", "--model", required=True, help="output .cpickle file")
args = vars(ap.parse_args())

def train_from_hdf5(args):
    # load in the database
    db = h5py.File(args['database'], 'r')
    train_end = int(db['labels'].shape[0]*0.75)

    # initialize the estimator
    estimators = [('pca',PCA()),('logistic',LogisticRegression())]
    pipe = Pipeline(steps=estimators)
    params = dict(logistic__C = [0.1, 1.0, 0.001, 0.0001, 0.01], pca__n_components=[100, 1000, 10000])
    model = GridSearchCV(pipe, param_grid=params, cv=3, scoring='accuracy',n_jobs=args['jobs'])

    # fit the pca
    print('[INFO] fitting pca...')
    ipca = IncrementalPCA(n_components=10000, batch_size=16) # n_components must be not larger than min(chunkSize, n_features)
    chunkSize = 500
    for chunkStart in np.arange(0, train_end, chunkSize):
        chunkEnd = chunkStart + chunkSize
        if chunkEnd > train_end:
            chunkEnd = train_end
        ipca.partial_fit(db['features'][chunkStart:chunkEnd])
    plt.figure(1, figsize=(4, 3))
    plt.clf()
    plt.axes([.2, .2, .7, .7])
    plt.plot(ipca.explained_variance_ratio_, linewidth=2)
    plt.axis('tight')
    plt.xlabel('n_components')
    plt.ylabel('explained_variance_ratio_')
    print(np.sum(ipca.explained_variance_ratio_))
    plt.show()

    
    # fit the model
    t0 = time()
    print('[INFO] grid searching the best model...')
    model.fit(db['features'][:train_end],db['labels'][:train_end])
    print(f'[INFO] Time cost: {time()-t0}')
    print(f'[INFO] the best param of the model is {model.best_params_}')

    # make predictions
    print('[INFO] evaluating the model...')
    preds = model.predict(db['features'][train_end:])
    from sklearn.metrics import accuracy_score
    print(accuracy_score(db['labels'][train_end:],preds)+'\n')

    # serialize the model
    print('[INFO] saving the model...')
    with open(args['model'], 'wb') as wf:
        pickle.dump(model.best_estimator_,wf)

    # don't forget to close the database
    db.close()

if __name__ == '__main__':
    train_from_hdf5(args)