import pickle

import cv2
import os
from argparse import ArgumentParser

import face_recognition
from imutils import paths

ap = ArgumentParser()
ap.add_argument("-f", "--features", required=True, help="cpickle file with extracted features to be saved")
ap.add_argument("-m", "--method", required=True, help="face detection method")
ap.add_argument("-d", "--dataset", required=True,
                help="path to the downloaded dataset")
args = vars(ap.parse_args())

# load in images
imagePaths = list(paths.list_images(args['dataset']))
print(f'[INFO] load in {len(imagePaths)} images...')
names = [p.split(os.path.sep)[-2] for p in imagePaths]
known_names = []
known_features = []
print('[INFO] extracting features...')
for i,p in enumerate(imagePaths):
    image = cv2.imread(p)
    RGB = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    boxes = face_recognition.face_locations(RGB, model=args['method'])
    encodings = face_recognition.face_encodings(RGB, boxes)
    # assume that all the faces in one picture belong to the same person
    for e in encodings:
        known_features.append(e)
        known_names.append(names[i])

print('[INFO] serializing features...')
data = dict(features=known_features, names=known_names)
file_name = os.path.sep.join([args['features'], 'face_features.pickle'])
with open(file_name, 'wb') as wf:
    wf.write(pickle.dumps(data))