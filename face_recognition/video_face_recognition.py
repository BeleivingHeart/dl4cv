import pickle
import numpy as np
import cv2
from argparse import ArgumentParser
from time import sleep, time

import face_recognition
from imutils.video import VideoStream
from sklearn.externals import joblib

# ap = ArgumentParser()
# ap.add_argument("-f", "--features", required=True, help="cpickle file with extracted features to be loaded")
# ap.add_argument("-m", "--method", required=True, help="face detection method")
# ap.add_argument("-v", "--video", required=True,
#                 help="path to the video")
# args = vars(ap.parse_args())

# config VideoStream
video = r"E:\OneDrive\local-git\image-datasets\face_recognition\lunch_scene.mp4"
vs = VideoStream(src=video, resolution=(640, 352)).start()
sleep(2) # wait 2 seconds for the VideoStream to warm up
# config VideoWriter
outPath = r"E:\OneDrive\local-git\image-datasets\face_recognition\recognition.avi"
fourcc = cv2.VideoWriter_fourcc(*'MJPG')
writer = None

# load in names
dataFile = r"E:\OneDrive\local-git\model-and-performance\face_recognition\face_features.pickle"
with open(dataFile, 'rb') as rf:
    data = pickle.load(rf)
names = np.unique(data['names'])
# load in model
modelFile = r"E:\OneDrive\local-git\model-and-performance\face_recognition\SVCmodel.pkl"
with open(modelFile, 'rb') as rf:
    model = joblib.load(rf)

t0 = time()
while(True):
    frame = vs.read()
    if writer is None and outPath is not None:
        writer = cv2.VideoWriter(outPath, fourcc, 20, (frame.shape[1], frame.shape[0]), True)
    if type(frame) is not np.ndarray:
        print('[INFO] END OF VIDEO')
        break
    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    boxes = face_recognition.face_locations(rgb, model='hog')
    if not boxes:
        continue
    features = face_recognition.face_encodings(rgb, boxes)
    features = np.array(features)
    preds = model.predict(features)
    for b, p in zip(boxes, preds):
        name = names[p]
        pt1, pt2 = (b[3], b[0]), (b[1], b[2])
        cv2.rectangle(frame, pt1, pt2, color=(0,255,0), thickness=2)
        cv2.putText(frame, name, (pt1[0]-5, pt1[1]-5),fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.75, color=(0,255,0), thickness=2)
    cv2.imshow('Video', frame)
    writer.write(frame)
    key = cv2.waitKey(1)&0xff
    if key == ord('q'):
        break
    print(f'Elapsed time {time()-t0:0.2f} seconds')

cv2.destroyAllWindows()
writer.release()
vs.stop()