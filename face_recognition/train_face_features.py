import os
import pickle
from argparse import ArgumentParser
from time import time

from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelBinarizer, LabelEncoder
from sklearn.svm import SVC

ap = ArgumentParser()
ap.add_argument("-f", "--features", required=True, help="cpickle file with extracted features to be loaded")
ap.add_argument("-m", "--model", required=True,
                help="path to the output model")
args = vars(ap.parse_args())

with open(args['features'],'rb') as rf:
    data = pickle.load(rf)
features, names = data['features'], data['names']
import numpy as np
features = np.array(features)
names = np.array(names)

# preprocess data
lb = LabelEncoder()
labels = lb.fit_transform(names)
# config estimator
estimators = [('clf', SVC())]
pipe = Pipeline(steps=estimators)
params = {
    'clf':[SVC(), LogisticRegression()],
    'clf__C':np.logspace(-1,3,4)
}
model = GridSearchCV(pipe,param_grid=params,scoring='accuracy')


# fit the data
t0 = time()
print('[INFO] fitting data...')
model.fit(features, labels)
print(f'[INFO] elapsed time: {time()-t0}')
print(f'[INFO] best training score: {model.best_score_}')
print(f'[INFO] best params: {model.best_params_}')

# serialize model
print('[INFO] serializing model...')
modelPath = os.path.sep.join([args['model'], 'SVCmodel.pkl'])
with open(modelPath, 'wb') as wf:
    joblib.dump(model.best_estimator_, wf)
