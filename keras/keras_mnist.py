from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import SGD
from sklearn.datasets.mldata import fetch_mldata
import matplotlib.pyplot as plt
import numpy as np
import argparse

my_mnist_digits = "E:\OneDrive\local-git\dl4cv\keras"
ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", required=True,
                help="output model hdf5 file")
ap.add_argument("-e", "--epochs", type=int, default=10,
                help="training epochs")
args = vars(ap.parse_args())

print("[INFO] loading the mnist dataset")
dataset = fetch_mldata("MNIST Original", data_home=my_mnist_digits)
data = dataset.data.astype("float")/255.0
(trainX, testX, trainY, testY) = train_test_split(
    data, dataset.target, test_size=0.25)
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)

model = Sequential()
model.add(Dense(256, input_shape=(784,), activation="sigmoid"))
model.add(Dense(128, activation="sigmoid"))
model.add(Dense(10, activation="softmax"))

print("[INFO] training the network...")
sgd = SGD(0.01)
model.compile(loss="categorical_crossentropy",
              metrics=["accuracy"], optimizer=sgd)
H = model.fit(trainX, trainY, validation_data=(testX, testY),
              epochs=args["epochs"], batch_size=128)

print("[INFO] evaluating the network...")
predictions = model.predict(testX, batch_size=128)
print(classification_report(testY.argmax(axis=1), predictions.argmax(
    axis=1), target_names=[str(s) for s in lb.classes_]))

model.save(args["output"])
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, args["epochs"]), H.history["val_acc"], label="val_acc")
plt.title("Training loss and accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.show()
