import cv2
import numpy as np
imagePath = r"E:\OneDrive\local-git\image-datasets\face_recognition\alan_grant\00000006.jpg"
image = cv2.imread(imagePath)
lower = np.array([0, 48, 80], dtype="uint8")
upper = np.array([20, 255, 255], dtype="uint8")
hsv = cv2.cvtColor(image, code=cv2.COLOR_BGR2HSV)
mask = cv2.inRange(hsv, lower, upper)
kernel = cv2.getStructuringElement(shape=cv2.MORPH_ELLIPSE, ksize=(11,11))
mask = cv2.erode(mask, kernel, iterations=2)
mask = cv2.dilate(mask, kernel, iterations=2)/255.0
mask = np.ceil(mask).astype('uint8')
for i in range(image.shape[2]):
    image[:,:,i] *= mask
cv2.imshow('image',image)
cv2.waitKey(0)
cv2.destroyAllWindows()