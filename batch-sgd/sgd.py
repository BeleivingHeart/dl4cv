from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy as np
import argparse


def sigmoid_activation(x):
    return 1.0 / (1 + np.exp(-x))


def predict(X, W):
    preds = sigmoid_activation(X.dot(W))
    preds[preds <= 0.5] = 0
    preds[preds >= 0.5] = 1
    return preds

def next_batch(X, y, batchSize):
    for i in np.arange(0, X.shape[0], batchSize):
        yield (X[i:i + batchSize], y[i:i+batchSize])


ap = argparse.ArgumentParser()
ap.add_argument("-e", "--epochs", type=float, default=100, help="# of epochs")
ap.add_argument("-a", "--alpha", type=float,
                default=0.01, help="learning rate")
ap.add_argument("-b", "--batch_size", type=int, default=32,
                help="size of SGD mini-batches")
args = vars(ap.parse_args())

# initilize train and test dataset
(coordinates, lables) = make_blobs(n_samples=1000, n_features=2,
                                   centers=2, cluster_std=1.5, random_state=1)
lables = lables.reshape((lables.shape[0], 1))

coordinates = np.c_[coordinates, np.ones(coordinates.shape[0])]

(train_coordinates, test_coordinates, train_labels, test_labels) = train_test_split(
    coordinates, lables, test_size=0.5, random_state=42)

print("[INFO] training...")
W = np.random.randn(coordinates.shape[1], 1)
losses = []

for epoch in np.arange(args["epochs"]):
    epochLoss = []
    batches = next_batch(train_coordinates, train_labels, args["batch_size"])
    for(batchX, batchY) in batches:
        preds = sigmoid_activation(batchX.dot(W))
        error = preds - batchY
        epochLoss.append(np.sum(error**2))
        gradient = batchX.T.dot(error)
        W += -args["alpha"]*gradient 
    loss = np.sum(epochLoss)
    losses.append(loss)
    if epoch == 0 or (epoch+1) % 5 == 0:
        print("[INFO] epoch = {}, loss ={:.7f}".format(int(epoch + 1), loss))

print("[INFO] evaluating...")
preds = predict(test_coordinates, W)
print(classification_report(test_labels, preds))


plt.style.use("ggplot")
plt.figure()
plt.title("Data")
plt.scatter(test_coordinates[:, 0],
            test_coordinates[:, 1], s=30, c=test_labels.flatten())

plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), losses)
plt.title("Training Loss")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.show()
