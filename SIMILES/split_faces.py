if __name__ == "__main__":
    pre_path = "E:\OneDrive\local-git\image-datasets\SMILES\\faces\\"
    smile_path = "E:\OneDrive\local-git\image-datasets\SMILES\positives\\"
    non_smile_path = 'E:\OneDrive\local-git\image-datasets\SMILES\\negetives\\'
    # the txt file has only 600 lines
    with open('SMILE_list.txt','r') as rf:
        for line in rf:
            # delete /n at the line end
            line = line[:-1]
            path = pre_path + line
            with open(path, 'rb') as src:
                with open(smile_path + line, 'wb') as dst:
                    dst.write(src.read())

    with open('NON-SMILE_list.txt','r') as rf:
        for line in rf:
            line = line[:-1]
            path = pre_path + line
            with open(path, 'rb') as src:
                with open(non_smile_path + line, 'wb') as dst:
                    dst.write(src.read())