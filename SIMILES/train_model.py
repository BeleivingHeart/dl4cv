from sklearn.preprocessing import LabelEncoder
import argparse
from imutils import paths
import os
import cv2
from keras.preprocessing.image import img_to_array
from keras.utils import np_utils
import numpy as np
from lenet import LetNet
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True, help="path to input dataset")
ap.add_argument("-m", "--model", required=True, help=".h5 model to be saved")
ap.add_argument("-e", "--epochs",  type=int,
                default=40, help="training epochs")
args = vars(ap.parse_args())

# load the data
imagePaths = sorted(list(paths.list_images(args['dataset'])))
data = []
labels = []
for path in imagePaths:
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    image = img_to_array(image)
    data.append(image)
    label = path.split(os.path.sep)[-2]
    label = "smile" if label == 'positives' else 'not_smile'
    labels.append(label)
data = np.array(data)/255.0
labels = np.array(labels)

# preprocess the data and labels
le = LabelEncoder().fit(labels)
labels = np_utils.to_categorical(le.transform(labels), 2)
classesTotal = labels.sum(axis=0)
classWeight = classesTotal.max()/classesTotal
(trainX, testX, trainY, testY) = train_test_split(
    data, labels, test_size=0.2, stratify=labels, random_state=42)  # stratify ensures that both the train and test sets will have the the same propotion of various classes

# build and compile the model
print('[INFO] building the network...')
model = LetNet.build(height=64, width=64, depth=1, classes=2)
model.compile(optimizer='adam', loss='binary_crossentropy',
              metrics=['accuracy'])

# fit and save the network
print('[INFO] training the network...')
outPath = args["model"]+'\SIMILES.h5'
checkPoint = ModelCheckpoint(
    outPath, monitor="val_loss", verbose=1, save_best_only=True)
callbacks = [checkPoint]
H = model.fit(trainX, trainY, validation_data=(testX, testY),# use class_weight to balance the skewed data
              epochs=args['epochs'], batch_size=64, class_weight=classWeight, verbose=1,callbacks=callbacks)
              
# evaluate the network
print("[INFO] Evaluating the model...")
predictions = model.predict(testX, batch_size=64)
print(classification_report(testY.argmax(axis=1), predictions.argmax(
    axis=1), target_names=le.classes_))

plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["epochs"]), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, args["epochs"]), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, args["epochs"]), H.history["val_acc"], label="val_acc")
plt.title("Training loss and accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig(args["model"]+"\SIMILES.png")
plt.show()

