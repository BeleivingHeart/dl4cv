""""
This file converts pgm images to jpgs in the same given directory
"""
from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-i", "--images", required=True,
                help="path to the dir of images")
args = vars(ap.parse_args())


def pgmConverter(images):
    # in_dir == out_dir
    import os, glob
    from PIL import Image
    if not os.path.exists(images):
        print('Error: path of images does not exists!')
        return -1
    count = 1
    for pgm in glob.glob(images+'/*'):
        if 'pgm' in pgm:
            image = Image.open(pgm)
            path_parts = pgm.split(os.path.sep)
            name = path_parts[-1]
            name = name.replace('pgm','jpg')
            image.save(images+'/'+name)
            print(f'[INFO] {count} images converted...')
            count+=1



if __name__ == "__main__":
    pgmConverter(args["images"])