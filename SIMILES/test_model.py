from argparse import ArgumentParser

ap = ArgumentParser()
ap.add_argument("-m", "--model", required=True,
                help=".h5 model to be loaded")
ap.add_argument("-c", "--cascade", required=True,
                help="epochs to be trained")
ap.add_argument("-v", "--video", help="face video to be loaded")
args = vars(ap.parse_args())


def test_model(args):
    # initialize things
    import numpy as np
    import cv2
    from keras.models import load_model
    from keras.preprocessing.image import img_to_array
    from imutils import resize

    cascade = cv2.CascadeClassifier(args['cascade'])
    if not args.get('video', False):
        camera = cv2.VideoCapture(0)
    else:
        camera = cv2.VideoCapture(args['video'])
    model = load_model(args['model'])

    while True:
        grabbed, frame = camera.read()
        frame = resize(frame, width=300)
        if not grabbed:
            break
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame_cloned = frame.copy()
        faces = cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5,
                                         flags=cv2.CASCADE_SCALE_IMAGE, minSize=(30, 30), maxSize=(200, 200))
        for (x, y, w, h) in faces:
            roi = gray[y:y+h, x:x+w]
            roi = cv2.resize(roi,(64,64))
            cv2.rectangle(frame_cloned, (x, y), (x+w, y+h),
                          (0, 255, 0), thickness=2)
            roi = img_to_array(roi/255.0)
            roi = np.expand_dims(roi, axis=0)
            [not_smile, smile] = model.predict(roi)[0]
            label = 'smile' if smile > not_smile else 'not_smile'
            cv2.putText(frame_cloned, label, (x, y - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

        cv2.imshow('faces', frame_cloned)
        if cv2.waitKey(5) & 0xFF == ord('q'):
            print('[INFO] manually break from video')
            break

    camera.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    test_model(args)
